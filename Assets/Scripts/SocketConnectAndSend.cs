﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

namespace TobiiGlasses
{

    public class SocketConnectAndSend : MonoBehaviour
    {


        static int liveCtrlPort = 49152;
        static IPAddress ipGlasses = IPAddress.Parse("192.168.71.50");
        public static IPEndPoint ipEndPoint = new IPEndPoint(ipGlasses, liveCtrlPort);

        // Création des sockets
        public static Socket socketData = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        public static Socket socketVideo = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);


        public int i = 0;

        // Start is called before the first frame update
        void Start()
        {
            socketData.Connect(ipEndPoint);
            socketVideo.Connect(ipEndPoint);
            SendKeepAliveMessage.SendKAM(ipEndPoint,socketData,socketVideo); 
        }

        // Update is called once per frame
        void Update()
        {
            i++;
            //if(i==1000)
            {
                SendKeepAliveMessage.SendKAM(ipEndPoint,socketData,socketVideo);
                i = 0;

            }
        }
    }

}