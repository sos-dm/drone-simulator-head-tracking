﻿using System;

namespace TobiiGlasses
{
	public class ConvertPCData
	{
		public class Data { }
		public class PupilCenter : Data
		{
			public string pc { get; set; }
			public string eye{ get; set;}
			public PupilCenter (JSONObject jo)
			{
				this.pc = jo.GetField ("pc").ToString ();
				this.eye = jo.GetField("eye").ToString();
			}
		}

		public static float[] PCData(string dataReceivedString){
			JSONObject jobject = new JSONObject(dataReceivedString);
			PupilCenter pupilCenter = new PupilCenter(jobject);
			String coordonnee = pupilCenter.pc.Replace("[", "").Replace("]", "").Replace(" ", "");
			string[] stringSeparators = new string[] { "," };
			string[] result = coordonnee.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
			string xString = (string)result.GetValue(0);
			string yString = (string)result.GetValue(1);
			string zString = (string)result.GetValue(2);
			float x = Convert.ToSingle(xString);
			float y = Convert.ToSingle(yString);
			float z = Convert.ToSingle(zString);
			float[] output = new float[3];
			output[0] = x;
			output[1] = y;
			output[2] = z;
			return output;
		}
	}
}

