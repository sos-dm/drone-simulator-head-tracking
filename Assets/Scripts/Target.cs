﻿using System;
using System.Net;
using System.Net.Sockets;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TobiiGlasses
{
    public class Target : MonoBehaviour
    {
        public float smoothTime = 0.3F;

       // public float smoothTime = 10F;

        private Vector3 velocity = Vector3.zero;

        public float facteurmult = 100.0f;

        static int liveCtrlPort = 49152;
        static IPAddress ipGlasses = IPAddress.Parse("192.168.71.50");
        static IPEndPoint ipEndPoint = new IPEndPoint(ipGlasses, liveCtrlPort);
        // Création des sockets
        static Socket socketData = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        static Socket socketVideo = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

        public int i = 0;

        // Use this for initialization
        void Start()
        {
            socketData.Connect(ipEndPoint);
            socketVideo.Connect(ipEndPoint);
            SendKeepAliveMessage.SendKAM(ipEndPoint, socketData, socketVideo);
        }

        void Update()
        {
			i++;
			if (i == 120) {
				SendKeepAliveMessage.SendKAM (ipEndPoint, socketData, socketVideo);
				i = 0;
			}

            string dataReceiveString = ReceiveData.RData(socketData);
			while (!dataReceiveString.Contains("gp3"))
            {
                dataReceiveString = ReceiveData.RData(socketData);
            }
			//SendKeepAliveMessage.SendKAM(ipEndPoint, socketData, socketVideo);
            if (dataReceiveString.Contains("gp3"))
            {
                float[] position = new float[3];
                position = ConvertGP3Data.CData(dataReceiveString);
                if (position[1] != 0 && position[2] != 0 && position[0] != 0/*position[1] + position[2] + position[0] != 0*/)
                {
                    Vector3 camPosition = GameObject.FindGameObjectWithTag("MainCamera").transform.position;
					//Vector3 newPosition = new Vector3(-position[0]/100f + camPosition.x, position[1]/100f + camPosition.y, position[2]/100f + camPosition.z);
                    //fixed z position
                    Vector3 newPosition = new Vector3(-position[0] / facteurmult, position[1] / facteurmult, /*position[2] / facteurmult*/transform.localPosition.z);

                    //Vector3 newPosition = new Vector3(-position[0] / facteurmult, position[1] / facteurmult, position[2] / facteurmult);

                    //transform.position = Vector3.SmoothDamp(transform.position, newPosition, ref velocity, smoothTime);
                    transform.localPosition = Vector3.SmoothDamp(transform.localPosition, newPosition, ref velocity, smoothTime);

                    //gestion du déplacement fluide
                   // transform.localPosition = Vector3.SmoothDamp(transform.localPosition, newPosition, ref velocity, smoothTime * Time.deltaTime);


                    //Debug.Log("x : " + position[0] + " y : " + position[1] + " z : " + position[2]);
                }
            }
        }
    }
}
