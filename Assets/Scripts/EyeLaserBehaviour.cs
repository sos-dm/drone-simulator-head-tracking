﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

namespace TobiiGlasses{
	public class EyeLaserBehaviour : MonoBehaviour {

		// Création des sockets
		static int liveCtrlPort = 49152;
		static IPAddress ipGlasses = IPAddress.Parse("192.168.71.50");
		static IPEndPoint ipEndPoint = new IPEndPoint(ipGlasses, liveCtrlPort);

		static Socket socketData = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
		static Socket socketVideo = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

		private int i = 0;
		private float smoothTime = 0.3F;
		private Vector3 velocity = Vector3.zero;
		// Laser
		private Color laserColor = new Color (0f, 1f, 1f);

		private float thickness = 0.002f;
		private float infiniteLength = 100f;

		private GameObject holder;
		private GameObject pointer;

		// Use this for initialization
		void Start () {
			socketData.Connect(ipEndPoint);
			socketVideo.Connect(ipEndPoint);
			SendKeepAliveMessage.SendKAM(ipEndPoint, socketData, socketVideo);

			holder = new GameObject("Holder");
			holder.transform.parent = this.transform;
			holder.transform.localPosition = Vector3.zero;
			holder.transform.localRotation = Quaternion.identity; // added

			pointer = GameObject.CreatePrimitive(PrimitiveType.Cube);
			pointer.name = "Pointer"; // added
			pointer.transform.parent = holder.transform;
			pointer.GetComponent<MeshRenderer>().material.color = laserColor;
		}
		
		// Update is called once per frame
		void Update () {
			// Send KAM every 2 seconds
			i++;
			if (i == 120) {
				SendKeepAliveMessage.SendKAM (ipEndPoint, socketData, socketVideo);
				i = 0;
			}

			// Wait for 3D Gaze Data
			string dataReceiveString = ReceiveData.RData(socketData);
			while (!dataReceiveString.Contains("gp3"))
			{
				dataReceiveString = ReceiveData.RData(socketData);
			}

			if (dataReceiveString.Contains("gp3"))
			{
				float[] position = new float[3];
				position = ConvertGP3Data.CData(dataReceiveString);
				if (position[1] + position[2] + position[0] != 0)
				{
					Vector3 camPosition = GameObject.FindGameObjectWithTag("MainCamera").transform.position;
					Vector3 newPosition = new Vector3(-position[0]/100f - camPosition.x, position[1]/100f - camPosition.y, position[2]/100f - camPosition.z);
					Vector3 direction = Vector3.SmoothDamp(transform.position, newPosition, ref velocity, smoothTime);

					// Compute Ray direction
					pointer.transform.localScale = new Vector3(thickness, thickness, newPosition.magnitude);
					pointer.transform.localPosition = new Vector3(0f, -0.5f, -0.5f);
					pointer.transform.localRotation = Quaternion.LookRotation (direction);
				}

			}
		}
	}
}

