﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

namespace TobiiGlasses
{ 
    public class GazePositionCapture_New : MonoBehaviour
    {
        //public Transform target;
        public float smoothTime = 0.3F;
       // public float smoothTime = 0.0F;
        private Vector3 velocity = Vector3.zero;
        // public UnityEngine.UI.Text uitext;
        /*alex modif*/
        public static float[] recordData;
        public static float[] recordUnityData;
        
        private int i = 0;
        public float facteurmult = 1000f;
        //public float facteurmultY = 100.0f;
       
        public float correctX = 0;
        public float correctY = 0;
        public float correctZ = 0;

        
       
        // Start is called before the first frame update
        void Start()
        {
            recordData = new float[3];
            recordUnityData = new float[3];
                       
        }

        // Update is called once per frame

        void Update()
        {
            i++;
            if (i == 10)
            {
               // SendKeepAliveMessage.SendKAM(SocketConnectAndSend.ipEndPoint, SocketConnectAndSend.socketData, SocketConnectAndSend.socketVideo);
                i = 0;
            }


            string dataReceiveString = ReceiveData.RData(SocketConnectAndSend.socketData);
            while (!dataReceiveString.Contains("gp3"))
            {
                dataReceiveString = ReceiveData.RData(SocketConnectAndSend.socketData);
            }
			//SendKeepAliveMessage.SendKAM(ipEndPoint, socketData, socketVideo);
            if (dataReceiveString.Contains("gp3")) /*(dataReceiveString.Contains("gp") && !dataReceiveString.Contains("gp3"))*/
            {
                float[] position = new float[3];
                float[] regard = new float[3];
                ConvertGP3Data_New.DataConverted dataConverted;
                Reperes.GazePoint gazePoint;

                //position = GazePositionCoordinates(ConvertGP3Data.CData(dataReceiveString));
                //regard = ConvertGP3Data.CData(dataReceiveString);

               // position = GazePositionCoordinates(ConvertGP3Data.CData(dataReceiveString));
                dataConverted = ConvertGP3Data_New.CData(dataReceiveString);
                gazePoint = GazePositionCoordinates(dataConverted);


               // recordData = ConvertGP3Data.CData(dataReceiveString);
               // if (position[1] + position[2] + position[0] != 0)
               // if (position[1]!=0 && position[2]!=0 && position[0] != 0)

                    {

                    //Avec le z fixé.
                    //Vector3 newPosition = new Vector3((-position[0] / facteurmult)-0.5f + camPosition.x, (position[1] / facteurmultY)+0.5f + camPosition.y, /*(position[2] / facteurmultY) + camPosition.z*/0);
                    //Vector3 newPosition = new Vector3((-position[0] / facteurmult) + camPosition.x, (position[1] / facteurmultY) + camPosition.y, transform.position.z);
                    //Vector3 newPosition = new Vector3((-position[0] / facteurmult), (position[1] / facteurmult), (position[2] / facteurmult));
                    //Vector3 newPosition = new Vector3((-position[0] / facteurmult), (position[1] / facteurmult), (position[2] / facteurmult));
                   // Vector3 newPosition = new Vector3(gazePoint.Screen.x/facteurmult, gazePoint.Screen.y/facteurmult, 0);
                    Vector3 newPosition = new Vector3(gazePoint.Screen.x / facteurmult, gazePoint.Screen.y / facteurmult, 0);


                    ecrireFichier("LA CHAINE----------------- " + dataReceiveString);
                    ecrireFichier("Nouvelle position  ----------------- "+ newPosition.ToString());

                    transform.position = Vector3.SmoothDamp(transform.position, newPosition, ref velocity, smoothTime);
                    //transform.localPosition = Vector3.SmoothDamp(transform.localPosition, newPosition, ref velocity, smoothTime);

                //float x = transform.localPosition.x;

                int i = 0;

                 }
			}
        }
        public static void ecrireFichier(string data)
        {
           File.AppendAllText("data.txt", data + "\r\n");
        }

        Reperes.GazePoint GazePositionCoordinates(ConvertGP3Data_New.DataConverted dataConverted)
        {
            Reperes.GazePoint gazePoint= Reperes.TobiiGlassesAPI.GetGazePoint(dataConverted); 
            return gazePoint;
        }

    }
}