﻿using System;
using System.Net;
using System.Net.Sockets;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace TobiiGlasses
{
	public class EyeControl : MonoBehaviour
	{
		static int liveCtrlPort = 49152;
		static IPAddress ipGlasses = IPAddress.Parse("192.168.71.50");
		static IPEndPoint ipEndPoint = new IPEndPoint(ipGlasses, liveCtrlPort);
		// Création des sockets
		static Socket socketData = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
		static Socket socketVideo = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

		public int i = 0;
		public int gCtr = 0;
		public int dCtr = 0;

		// Use this for initialization
		void Start()
		{
			socketData.Connect(ipEndPoint);
			socketVideo.Connect(ipEndPoint);
			SendKeepAliveMessage.SendKAM(ipEndPoint, socketData, socketVideo);
		}

		void Update()
		{
			i++;
			if (i == 120) {
				SendKeepAliveMessage.SendKAM (ipEndPoint, socketData, socketVideo);
				i = 0;
			}

			string dataReceiveString = ReceiveData.RData(socketData);
			while (!dataReceiveString.Contains("pd"))
			{
				dataReceiveString = ReceiveData.RData(socketData);
			}
			if(dataReceiveString.Contains("pd")){
				float pupilDiameter = ConvertPDData.PDData (dataReceiveString);
				Debug.Log (pupilDiameter);
				if (dataReceiveString.Contains ("left")) {
					if (pupilDiameter == 0) {
						Debug.Log ("Oeil gauche fermé");
						gCtr++;
					} else {
						gCtr = 0;
					}
				}
				if (dataReceiveString.Contains ("right")) {
					if (pupilDiameter == 0) {
						Debug.Log ("Oeil droit fermé");
						dCtr++;
					} else {
						dCtr = 0;
					}
				}
			}

			/*while (!dataReceiveString.Contains("pc"))
			{
				dataReceiveString = ReceiveData.RData(socketData);
			}
			if (dataReceiveString.Contains ("pc")) {
				float[] pc = ConvertPCData.PCData (dataReceiveString);
				if(dataReceiveString.Contains("left"))
					Debug.Log ("Left : " + pc [0] + " : " + pc [1] + " : " + pc [2]);
				//else
					//Debug.Log ("Right : " + pc [0] + " - " + pc [1] + " - " + pc [2]);
			}*/
			if(gCtr > 50)
				gameObject.GetComponent<Renderer>().material.color = Color.red;
			if(dCtr > 50)
				gameObject.GetComponent<Renderer>().material.color = Color.green;
		}
	}
}
