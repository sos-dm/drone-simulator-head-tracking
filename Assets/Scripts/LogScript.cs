using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class LogScript : MonoBehaviour {
    private StreamWriter _logWriter;
    private StreamWriter _logUnityWriter;
    private string _userName;
	public bool isLog= false;
    public bool isUnityLog = false;

    private float[] dataRecorded;
    private float[] dataUnityRecorded= new float[3];
    // Use this for initialization
    void Start ()
    {
        dataRecorded = new float[3];
        _userName = System.DateTime.Now.Hour+"_"+ System.DateTime.Now.Minute+"_"+ System.DateTime.Now.Second;
        _logWriter = new StreamWriter(Path.Combine(Application.persistentDataPath, _userName + "log.csv"),true);
        _logUnityWriter = new StreamWriter(Path.Combine(Application.persistentDataPath, _userName+"Unity"+ "log.csv"), true);
        Debug.Log(Path.Combine(Application.persistentDataPath, _userName + "log.csv"));
        WriteLog(Time.time + ";" + "Init" + ";" + ";" + ";");
        WriteUnityLog(Time.time + ";" + "Init" + ";" + ";" + ";");
    }
    void OnDrawGizmosSelected()
    {
        // Draws a 5 unit long red line in front of the object
        Gizmos.color = Color.red;
        Vector3 direction = new Vector3(TobiiGlasses.GazePositionCapture.recordUnityData[0], TobiiGlasses.GazePositionCapture.recordUnityData[1],TobiiGlasses.GazePositionCapture.recordUnityData[2]);
        Gizmos.DrawRay(transform.position, direction);
    }
    public void WriteLog(string s)
    {
        _logWriter.WriteLine(s);
        _logWriter.Flush();
    }
    public void WriteUnityLog(string s)
    {
        _logUnityWriter.WriteLine(s);
        _logUnityWriter.Flush();
    }
    private void OnDisable()
    {
        _logWriter.Close();
        _logUnityWriter.Close();
    }

	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            isLog = !isLog;
            isUnityLog = !isUnityLog;
        }

		if(isLog)
		{
            dataRecorded = TobiiGlasses.GazePositionCapture.recordData;
			WriteLog(Time.time + ";" + "position eye tracker;" + dataRecorded[0] + ";" + dataRecorded[1] + ";" + dataRecorded[2] + ";");
		}
        if (isUnityLog)
        {
            dataUnityRecorded = TobiiGlasses.GazePositionCapture.recordUnityData;
            WriteUnityLog(Time.time + ";" + "position unity;" + dataUnityRecorded[0] + ";" + dataUnityRecorded[1] + ";" + dataUnityRecorded[2] + ";");
        }
    }
}
