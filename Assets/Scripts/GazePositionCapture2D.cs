﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

namespace TobiiGlasses
{ 
    public class GazePositionCapture2D : MonoBehaviour
    {
        //public Transform target;
        public float smoothTime = 0.3F;
       // public float smoothTime = 0.0F;
        private Vector3 velocity = Vector3.zero;
        // public UnityEngine.UI.Text uitext;
        /*alex modif*/
        public static float[] recordData;
        public static float[] recordUnityData;
        private int i = 0;
        public float facteurmult = 100.0f;
        public float facteurmultY = 100.0f;
        public float correctX = 0;
        public float correctY = 0;
        public float correctZ = 0;
        // Start is called before the first frame update
        void Start()
        {
            recordData = new float[3];
            recordUnityData = new float[3];
        }

        // Update is called once per frame

        void Update()
        {

            i++;
            if (i == 120)
            {
                SendKeepAliveMessage.SendKAM(SocketConnectAndSend.ipEndPoint, SocketConnectAndSend.socketData, SocketConnectAndSend.socketVideo);
                i = 0;
            }


            string dataReceiveString = ReceiveData.RData(SocketConnectAndSend.socketData);
            
			while (!dataReceiveString.Contains("gp"))
            {
                dataReceiveString = ReceiveData.RData(SocketConnectAndSend.socketData);
            }
			//SendKeepAliveMessage.SendKAM(ipEndPoint, socketData, socketVideo);
            if (dataReceiveString.Contains("gp") && !dataReceiveString.Contains("gp3"))
            {
                float[] position = new float[2];
                position = ConvertGP2Data.CData(dataReceiveString);
                recordData = ConvertGP2Data.CData(dataReceiveString);
                // if (position[1] + position[2] + position[0] != 0)
                //  {
                Vector3 camPosition = GameObject.FindGameObjectWithTag("MainCamera").transform.position;
                    Vector3 spherePosition = GameObject.FindGameObjectWithTag("Sphere").transform.position;
                    Vector3 sphereLocalPosition = GameObject.FindGameObjectWithTag("Sphere").transform.localPosition;
                    Vector3 cubePosition = GameObject.FindGameObjectWithTag("Cube").transform.position;

                
                //Vector3 newPosition = new Vector3(-position[0] + camPosition.x, position[1] + camPosition.y, position[2] + camPosition.z);
                //ecrireFichier("====================================================================================");
                // Vector3 newPosition = new Vector3(-position[0] / 100f + camPosition.x, position[1] / 100f + camPosition.y, position[2] / 100f + camPosition.z);

                    ecrireFichier("=============================SANS SOMMER DONNEES CAMERA================================================");
                Vector3 viewporttest = new Vector3(1-position[0], position[1], 1);
                Vector3 correctedviewport = Vector3.zero;
                //Avec le z fixé.
                //Vector3 newPosition = new Vector3((-position[0] / facteurmult) + camPosition.x, (position[1] / facteurmultY) + camPosition.y, (position[2] / facteurmultY) + camPosition.z );
                //modif alex 12/04/19
                Vector3 CorrectedPos = Camera.main.ViewportToWorldPoint(correctedviewport+viewporttest);
                //recordUnityData[0] = newPosition[0];
                //recordUnityData[1] = newPosition[1];
                //recordUnityData[2] = newPosition[2];
                // Vector3 newPosition = new Vector3(-position[0]/100f, position[1] / 100f, transform.position.z);
                transform.position = Vector3.SmoothDamp(transform.position, CorrectedPos, ref velocity, smoothTime);
                if (Input.GetKeyDown(KeyCode.L))
                {
                    correctedviewport = new Vector3(0.5f, 0.5f, 1) - viewporttest;
                    Debug.Log(correctedviewport.x+" "+correctedviewport.y);
                }
                //float x = transform.localPosition.x;

                //Debug.Log("x : " + position[0] + " y : " + position[1] + " z : " + position[2]);

                //préparer le fichier des données----------------------------


                string data;
                    int i = 0;

                    
                    //if (Input.GetKeyDown(KeyCode.Space))
                    if ((Input.GetKeyDown(KeyCode.C)) || (Input.GetKeyDown(KeyCode.G)) || (Input.GetKeyDown(KeyCode.D)) || (Input.GetKeyDown(KeyCode.H)) || (Input.GetKeyDown(KeyCode.B)))

                    {
                        
                        if (Input.GetKeyDown(KeyCode.C)) { data = "---CENTRE---\r\n\n"; ecrireFichier(data);}
                        if (Input.GetKeyDown(KeyCode.G)) { data = "---GAUCHE---\r\n\n"; ecrireFichier(data); }
                        if (Input.GetKeyDown(KeyCode.D)) { data = "---DROITE---\r\n\n"; ecrireFichier(data); }
                        if (Input.GetKeyDown(KeyCode.H)) { data = "---HAUT---\r\n\n"; ecrireFichier(data); }
                        if (Input.GetKeyDown(KeyCode.B)) { data = "---BAS---\r\n\n"; ecrireFichier(data); }

                    // uitext.text = "NEW POSITION = x : " + newPosition.x + " y : " + newPosition.y + " z : " + newPosition.z;

                        ecrireFichier("====================================================================================");
                        data = "------CAMERA => x= " + camPosition.x + " || y= " + camPosition.y + " || z= " + camPosition.z;
                        ecrireFichier(data);
                        Debug.Log(data);
                        

                        data = "------REGARD (mm) => x= " + position[0] + " || y= " + position[1] + " || z= " + position[2];
                        string data1 = "------REGARD(mf) => x= " + position[0]/100f + " || y= " + position[1]/100f + " || z= " + position[2]/100f;
                        string data2 = "------REGARD(mf) => x= " + -position[0]/100f + " || y= " + position[1] / 100f + " || z= " + position[2] / 100f;
                        string data3 = "------REGARD(mf)+espison => x= " + -position[0]/100f + " || y= " + position[1] / 100f + " || z= " + position[2] / 100f;
                        Debug.Log(data);
                        ecrireFichier(data);
                        ecrireFichier(data1);
                        ecrireFichier(data2);
                        ecrireFichier(data3);



                    data = "------SPHERE => x= " + spherePosition.x + " || y= " + spherePosition.y + " || z= " + spherePosition.z;
                        Debug.Log(data);
                        ecrireFichier(data);

                        data = "------SPHERE (transforme)=> x= " + transform.position.x + " || y= " + transform.position.y + " || z= " + transform.position.z;
                        Debug.Log(data);
                        ecrireFichier(data);


                        data = "------LOCALE SPHERE => x= " + transform.localPosition.x + " || y= " + transform.localPosition.y + " || z= " + transform.localPosition.z;
                        Debug.Log(data);
                        ecrireFichier(data);

                        data = "------CUBE => x= " + cubePosition.x + " || y= " + cubePosition.y + " || z= " + cubePosition.z;
                        Debug.Log(data);
                        ecrireFichier(data);

                        Debug.Log("====================================================================================");
                        ecrireFichier("====================================================================================");
                    
                    // ecrireFichier(data);
                }
              //  }
			}
        }
        void ecrireFichier(string data)
        {
            //string path = @"data.txt";
           // StreamWriter fichier = new StreamWriter("data.txt");
            //fichier.WriteLine(data);
            //fichier.Close();

            //ou

            //FileStream fichier = File.Open("data.txt", FileMode.Append);
            //Byte[] info = new UTF8Encoding(true).GetBytes(data + "\r\n");
            //fichier.Write(info, 0, data.Length);
            //fichier.Close();

            
            File.AppendAllText("data.txt", data + "\r\n");
        }

    }
}