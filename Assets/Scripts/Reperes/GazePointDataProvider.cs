﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//public class GazePointDataProvider : MonoBehaviour
//-----------------------------------------------------------------------
// Copyright 2014 Tobii Technology AB. All rights reserved.
//-----------------------------------------------------------------------

namespace TobiiGlasses.Reperes
{
    /// <summary>
    /// Provider of gaze point data. When the provider has been started it
    /// will continuously update the Last property with the latest gaze point 
    /// value received from Tobii Engine.
    /// </summary>
    internal class GazePointDataProvider 
    {
     //   private readonly ITobiiHost _tobiiHost;
          //private static GazePoint Last = GazePoint.Invalid;

        /// <summary>
        /// Creates a new instance.
        /// Note: don't create instances of this class directly. Use the <see cref="TobiiHost.GetGazePointDataProvider"/> method instead.
        /// </summary>
        /// <param name="tobiiHost">Eye Tracking Host.</param>
        public GazePointDataProvider(/*ITobiiHost tobiiHost*/)
        {
            // _tobiiHost = tobiiHost;
            //GameViewInfo gameViewInfo = new GameViewInfo();
            //GazePoint Last = GazePoint.Invalid;
        }

        //protected override void UpdateData()
        //{
        //    var gazePoints = TobiiGameIntegrationApi.GetGazePoints();
        //    foreach (var gazePoint in gazePoints)
        //    {
        //        OnGazePoint(gazePoint);
        //    }
        //}

        public static GazePoint GetGazePointDataProvider( ConvertGP3Data_New.DataConverted dataConverted)
        {
            //long eyetrackerCurrentUs = gazePoint.TimeStampMicroSeconds; // TODO awaiting new API from tgi;
            //float timeStampUnityUnscaled = Time.unscaledTime - ((eyetrackerCurrentUs - gazePoint.TimeStampMicroSeconds) / 1000000f);
            GazePoint Last = GazePoint.Invalid;
            long eyetrackerCurrentUs = dataConverted.ts;
            float timeStampUnityUnscaled = Time.unscaledTime - ((eyetrackerCurrentUs - dataConverted.ts) / 1000000f);

            //var bounds = _tobiiHost.GameViewInfo.NormalizedClientAreaBounds;
            var bounds = GameViewInfo.DefaultGameViewInfo.NormalizedClientAreaBounds;

            if (float.IsNaN(bounds.x)
                || float.IsNaN(bounds.y)
                || float.IsNaN(bounds.width)
                || float.IsNaN(bounds.height)
                || bounds.width < float.Epsilon
                || bounds.height < float.Epsilon)
                return Last;

            var x = (0.5f + dataConverted.gp3[0] * 0.5f - bounds.x) / bounds.width;
            var y = (0.5f + dataConverted.gp3[1] * 0.5f - (1 - bounds.height - bounds.y)) / bounds.height;
            Last = new GazePoint(new Vector2(x, y), timeStampUnityUnscaled, dataConverted.ts);

            GazePositionCapture_New.ecrireFichier("X=" + x.ToString() + "\r\n");
            GazePositionCapture_New.ecrireFichier("y=" + y.ToString() + "\r\n");

            GazePositionCapture_New.ecrireFichier("Bounds=" + bounds.ToString() + "\r\n");

            return Last;
        }
                
    }
}