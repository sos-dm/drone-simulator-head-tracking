﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//-----------------------------------------------------------------------
// Gestion des données sur la fenetre de vue
//-----------------------------------------------------------------------

namespace TobiiGlasses.Reperes
{
    internal struct GameViewInfo
    {
        private readonly Rect _normalizedClientAreaBounds;

        public Rect NormalizedClientAreaBounds
        {
            get { return _normalizedClientAreaBounds; }
        }

        public static GameViewInfo DefaultGameViewInfo
        {
            get { return new GameViewInfo(new Rect(0f, 0f, 1f, 1f)); }
        }

        public GameViewInfo(Rect normalizedClientAreaBounds)
        {
            _normalizedClientAreaBounds = normalizedClientAreaBounds;
        }
    }
}