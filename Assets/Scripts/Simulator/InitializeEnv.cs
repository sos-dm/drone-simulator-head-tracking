﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class InitializeEnv : MonoBehaviour
{
    //public static GameObject[] _allDrones;
    GameObject droneModel; //le prefab cree à partir du l' objet original 
    


    public float dronesScale = 1.0f;
    //public float dronesSpeed = 5.0f;            //speed of movement of each drone 
    public float SpeedMissionDrones = 5.0f;            //------------ICI NEW----------------- 
    public float SpeedTransitDrones = 5.0f;            //------------ICI NEW----------------- 


    public int TotalNumberOfDrones = 3;
    public int numberDronesToTreat = 3;         //number of drones to treat
    public float stapeDuration = 3.0f;
    public float timeBetweenStatus = 3.0f;
    public float timeToMaintainColor = 1.0f;

    public string userCode;
    public ExperimentationTypes experimentationTypes;
    public ScenarioTypes Scenario;

    //gestion fichier--------------------------------------

    public static string appPath;
    public static string data = @"Data";
    public static string userDirectory;
    public static string scenario;
    public static string experience;

    public static float nbTotalDrone;
    public static float nbDroneToTreat;
    public static float speed;
    public static float durationStape;
    public static float delayBetweenStatus;
    public static float timeToColor;

    //----------------------------------------------------


    ActionsOnDrones actionsOnDrones = new ActionsOnDrones();

    private GameObject[] allDrones;
    //private GameObject drone;

    // Start is called before the first frame update
    void Start()
    {

        if (experimentationTypes != ExperimentationTypes.TouchAndHead)
        {
            UnityEngine.XR.XRSettings.enabled = false;
        }
        else
        {
            UnityEngine.XR.XRSettings.enabled = true;
        }

        // System.Console.WriteLine("Bonjour");

        // gestion fichier-------------------------
        appPath = Directory.GetCurrentDirectory();
        scenario = Scenario.ToString();
        experience = experimentationTypes.ToString();

        nbTotalDrone = TotalNumberOfDrones;
        nbDroneToTreat = numberDronesToTreat;
       

        initUserDirectory();
        //-----------------------------------------

        droneModel = GameObject.Find("Drone");

        //DroneAI.speed = dronesSpeed;         //----------ICI--------------
        DroneAI.droneScale = dronesScale;
        DroneAI.expeType = experimentationTypes;

        //-------------------ICI----------------
        droneModel.GetComponent<DroneAI>().speed = SpeedTransitDrones;
        droneModel.GetComponent<DroneAI>()._currentFunction = DroneFunction.inTransit;
        
        createDrones(TotalNumberOfDrones - numberDronesToTreat - 1, droneModel, SpeedTransitDrones, DroneFunction.inTransit);
        createDrones(numberDronesToTreat, droneModel, SpeedMissionDrones, DroneFunction.onMission);
         //--------------------------------------


        // createDrones(TotalNumberOfDrones-1, droneModel); //-------------------ICI------------------

              
        //_allDrones = getDrones();
        //int taille = _allDrones.Length;

        //actionsOnDrones.numberDronesToTreat = numberDronesToTreat;
        //actionsOnDrones.stapeDuration = stapeDuration;
        //actionsOnDrones.timeBetweenStatus = timeBetweenStatus;
        //actionsOnDrones.timeToMaintainColor = timeToMaintainColor;
       
        actionsOnDrones.initialize(numberDronesToTreat, stapeDuration, timeBetweenStatus, timeToMaintainColor, experimentationTypes);
        actionsOnDrones.Start();

        
    }

    // Update is called once per frame
    void Update()
    {
       //DroneAI.speed = dronesSpeed;
       DroneAI.droneScale = dronesScale;
       actionsOnDrones.Update();
             
    }

    private void OnDisable()
    {
        FileWriter.CloseActionWriter();
        FileWriter.CloseMissionSummarizeWriter();
    }

    private GameObject[] getDrones()
    {
        // charger le tableau des drones disponibles(pas encore traités)
        GameObject[] drones = GameObject.FindGameObjectsWithTag("Drone");
        return drones;
    }

    // create drones-----
    private void createDrones(int numberOfDroneToCreate, GameObject droneModel)
    {
        GameObject aDrone;
        Transform origine;

        int i;
        float x, y, z;
        Vector3 dronePositionOnTheScene;

        //pdroneModel = GameObject.Find("Drone");
        origine = droneModel.transform;
        
        for (i = 0; i < numberOfDroneToCreate; i++)
        {
            x = Random.Range(-3.5f, 10f);
            y = droneModel.transform.position.y;
            z = Random.Range(-4f, 9f);

            dronePositionOnTheScene = new Vector3(x, y, z);
            aDrone = Instantiate(droneModel, dronePositionOnTheScene, origine.rotation);
        
        }
    }

    //------------------------------------ICI-------------------------------------------
    private void createDrones(int numberOfDroneToCreate, GameObject droneModel, float speed, DroneFunction function)
    {
        GameObject aDrone;
        Transform origine;

        int i;
        float x, y, z;
        Vector3 dronePositionOnTheScene;

        //pdroneModel = GameObject.Find("Drone");
        origine = droneModel.transform;

        for (i = 0; i < numberOfDroneToCreate; i++)
        {
            //x = Random.Range(-21f, 21f);
            x = Random.Range(-16f, 16f);

            y = droneModel.transform.position.y;
            //z = Random.Range(-22f, 21f);
            //z = Random.Range(-6.5f, 9.5f);
            z = Random.Range(-8f, 9f);

            dronePositionOnTheScene = new Vector3(x, y, z);
            aDrone = Instantiate(droneModel, dronePositionOnTheScene, origine.rotation);
            aDrone.GetComponent<DroneAI>().speed = speed;
            aDrone.GetComponent<DroneAI>()._currentFunction = function;
        }
    }

    //--------------------------------------------------------------------------------------


    //for files management--------------------
    private void initUserDirectory()
    {
        //userDirectory = Path.Combine(Path.Combine(appPath, data, userCode, experimentationTypes.ToString()));
        userDirectory = Path.Combine(appPath, Path.Combine(data, Path.Combine(userCode, experimentationTypes.ToString())));

        Directory.CreateDirectory(userDirectory);
            
    }

}

//public enum Team
//{
//    Red,
//    Blue
//}

//public enum DroneState
//{
//    Wander,
//    Chase,
//    Attack
//}

//public enum DroneStatus
//{
//    Available,
//    Ready,
//    Treated,
//    Locked,
//    Completed
//}

//public struct TestItem
//{
//    public int id { get; set; }
//    public string text { get; set; }
    
//}

//public enum ExperimentationTypes
//{
//    Mouse =0,
//    MouseAndhHighlight,
//    Touch,
//    TouchAndhHead
//}