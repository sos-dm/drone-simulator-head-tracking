﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataStructures : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}


public enum Team
{
    Red,
    Blue
}

public enum DroneState
{
    Wander,
    Chase,
    Attack
}

public enum DroneStatus
{
    Available,
    Ready,
    Treated,
    Locked,
    Completed
}

public struct TestItem
{
    public int id { get; set; }
    public string text { get; set; }

}

public enum ExperimentationTypes
{
    Mouse = 0,
    MouseAndHighlight,
   // Mouse_Highligth_Zone,
    Touch,
    TouchAndHead,
   // Touch_Head_Zone
}

//----gestion fichier---------------
public enum ScenarioTypes
{
    Formation = 0,
   // Speed,
    DroneDensity,
    //Time,
   // DroneScale
}

public enum ActionStatu
{
    GOOD_SELECTION,
    BAD_SELECTION
}

//-----------------ICI NEW--------------------
public enum DroneSituation
{
    Pointed,
    outArea,
    inArea
}

public enum DroneFunction
{
    onMission,
    inTransit
}
//-------------------------------------------