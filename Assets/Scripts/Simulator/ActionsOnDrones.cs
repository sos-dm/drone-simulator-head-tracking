﻿//using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class ActionsOnDrones : MonoBehaviour
{
    public int numberDronesToTreat { get; set; }    // number of drones to treat
    public int numberDronesToManage { get; set; }   // number of all drones to manage
    public float stapeDuration { get; set; }
    public float timeBetweenStatus { get; set; }
    public float timeToMaintainColor { get; set; }
    public ExperimentationTypes expeType { get; set; }

    //public float dronesScale { get; set; }
    
    private float rayLength = 1000;

    private GameObject droneSelected;
    
    private float timerPrintEndMission = 3.0f;
    private float timerSelectNextDrone;
    private float timerLockDrone;

    private float timerColoredLockDrone;
    private float timerColoredTreatedDrone;
    
    private Color initialColor;
    private GameObject[] allDrones;

    private bool needSelection;
    private bool haveSelection;
    private bool isLockedDrone;
    private bool missionEnded;
    
    private int nbReadyAndAvailable = 0;
    private int nblockedDrones = 0;
    private float missionDuration = 0.0f;

    private int nbWrongSelection = 0;
    private int nbRightSelection = 0;
    private int nbAllSelection = 0;

    private GameObject previousHit = null;

    //For file management-------------------------------
    private float missionStart = 0.0f;
    private float missionEnd = 0.0f;
    private string missionSummarize;
    private string missionState;
    //--------------------------------------------------

    // private string rangeChoice = "";

    // bool firstEnter = true;

    // private GameObject[] availableDrones;

    //private GameObject[] currentAllDrones;

    // public LayerMask layerMask;

    // Start is called before the first frame update
    public void Start()
    {
        allDrones = GameObject.FindGameObjectsWithTag("Drone");
        initialColor = GameObject.FindGameObjectWithTag("Drone").GetComponent<Renderer>().material.color;


        needSelection = true;
        haveSelection = false;
        isLockedDrone = false;
        missionEnded = false;
       

        //nbReadyAndAvailable = nbAvailableDrones();
        nbReadyAndAvailable = numberDronesToTreat;
        
        timerSelectNextDrone = timeBetweenStatus;

        timerLockDrone = stapeDuration;

        timerColoredLockDrone = timeToMaintainColor;

        timerPrintEndMission = timeBetweenStatus;

        //availableDrones = getAvailableDrones();
        // currentAllDrones = allDrones;

        //gestion fichier-----------------------------

        FileWriter.initActionWriter(InitializeEnv.userDirectory, InitializeEnv.scenario + " " + InitializeEnv.nbTotalDrone);
        FileWriter.initSummarizeWriter(InitializeEnv.userDirectory, InitializeEnv.experience);
        missionStart += Time.deltaTime;
        //-----------------------------------------------

    }

    // Update is called once per frame
    public void Update()
    {
        if (missionEnded == false)
        {
            // ---- select next drone----------------
            if (needSelection == true && nbAvailableDrones() > 0)
            {
                timerSelectNextDrone -= Time.deltaTime;
                if (timerSelectNextDrone <= 0.0f && getLockedDrone() == null && getTreatedDrone() == null)
                {

                    SelectNextReadyDrone();
                }

            }

            // if (missionEnded == false)
            //if (expeType == ExperimentationTypes.Mouse || expeType == ExperimentationTypes.MouseAndHighlight)
            {
                clickedOnDrone();
            }

            if (expeType == ExperimentationTypes.TouchAndHead)
            {
               // Cursor.visible = false;
               // Cursor.lockState = CursorLockMode.Locked;
                changeDroneScaleWithHead();
                changeDroneScaleWithHeadinArea();
            }

            //----reset the treated drone color and status after a time------
            if (getTreatedDrone() != null)
            {
                timerColoredTreatedDrone -= Time.deltaTime;
                if (timerColoredTreatedDrone <= 0.0f)
                {
                    getTreatedDrone().GetComponent<Renderer>().material.color = initialColor;         // reset the treated drone colore
                    getTreatedDrone().GetComponent<DroneAI>()._currentStatus = DroneStatus.Available; // reset the treated drone status
                    timerSelectNextDrone = timeBetweenStatus;                                         // reset the time to select next drone
                    timerPrintEndMission = timeBetweenStatus;
                }
            }

            //------lock the drone in case of prolonged inactivity----
            if (/*haveSelection == true && needSelection == false*/getReadiedDrone())
            //if (haveSelection == true)
            {
                timerLockDrone -= Time.deltaTime;
                if (timerLockDrone <= 0.0f)
                {

                    lockDrone(droneSelected);
                    timerSelectNextDrone = timeBetweenStatus;    // -------ICI  temps selection : Ajout-------------
                    isLockedDrone = true;

                }

            }

            //----reset the locked drone colore and status after a time------
            if (getLockedDrone() != null)
            {
               timerColoredLockDrone -= Time.deltaTime;
                if (timerColoredLockDrone <= 0.0f)
                {
                    getLockedDrone().GetComponent<Renderer>().material.color = initialColor;          // reset the locked drone colore
                    getLockedDrone().GetComponent<DroneAI>()._currentStatus = DroneStatus.Available;  // reset the locked drone status
                    //timerSelectNextDrone = timeBetweenStatus;                                       // reset the time to select next drone -- ICI temps selection : retrait--- 
                    timerPrintEndMission = timeBetweenStatus;
                }
            }
            
            //-----end the mission--------------------------------
            if (nbReadyAndAvailable == 0 && missionEnded == false)
            {
                timerPrintEndMission -= Time.deltaTime;

                Debug.Log("timerPrintEndMission = " + timerPrintEndMission + " PRET= " + getReadiedDrone() + " BLOCKER= " + getLockedDrone() + " TRAITER= " + getTreatedDrone());
                
                if (timerPrintEndMission <= 0.0f && isSelectedDrone() == false)
                {
                    endMission();

                    missionEnd += missionDuration + Time.deltaTime;

                    missionSummarize = InitializeEnv.scenario + ";" + InitializeEnv.nbTotalDrone + ";" + InitializeEnv.nbDroneToTreat + ";" + missionStart + ";" + missionEnd + ";" + missionDuration + ";" + nbAllSelection + ";" + nbRightSelection + ";" + nbWrongSelection + ";" + missionState + ";";
                    FileWriter.WriteMissionSummarize(missionSummarize);
                }

            }
            //-------compute the mission duration-----------------
           // if (missionEnded == false)
            {
                missionDuration += Time.deltaTime;
            }

        }
    }
    //-----initialization of parameters----------
    public void initialize(int pNumberDronesToTreat, float pStapeDuration, float pTimeBetweenStatus, float pTimeToMaintainColor, ExperimentationTypes pExperimentationTypes)
    {
        numberDronesToTreat = pNumberDronesToTreat;
        stapeDuration = pStapeDuration;
        timeBetweenStatus = pTimeBetweenStatus;
        timeToMaintainColor = pTimeToMaintainColor;
        expeType = pExperimentationTypes;
        if (expeType != ExperimentationTypes.TouchAndHead)
        {
            //GameObject.Find("Head").SetActive(true);
            GameObject.Find("HeadDirection").SetActive(false);
        }
    }
   
    //------------------------------------------
    
    //gestion du clique sur un drone---------------
    private void clickedOnDrone()
    {
        if (Input.GetMouseButtonDown(0))
        {
            nbAllSelection++;

            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
           // if (Physics.Raycast(ray, out hit, rayLength, layerMask)
            if (Physics.Raycast(ray, out hit, rayLength))
            {
                //if (hit.collider.tag == "Drone" && hit.collider.GetComponent<DroneAI>()._currentStatus == DroneStatus.Ready)
                if (hit.collider.tag == "Drone")
                {
                    if (hit.collider.GetComponent<DroneAI>()._currentStatus == DroneStatus.Ready)
                    {
                        //Debug.Log("AVANT TRAITEMENT -----------------");
                        //Debug.Log("nbAvailableDrones    = " + nbAvailableDrones());
                        //Debug.Log("nbReadyAndAvailable  = " + nbReadyAndAvailable);
                        //Debug.Log(" needSelect=" + needSelection + " && dispo=" + nbAvailableDrones().ToString());

                        TreateDrone(hit);

                        nbRightSelection++;

                        FileWriter.WriteScenarioAction(missionDuration, ActionStatu.GOOD_SELECTION.ToString());

                        //Debug.Log("APRES TRAITEMENT -----------------");
                        //Debug.Log("nbAvailableDrones    = " + nbAvailableDrones());
                        //Debug.Log("nbReadyAndAvailable  = " + nbReadyAndAvailable);
                        //Debug.Log(" needSelect=" + needSelection + " && dispo=" + nbAvailableDrones().ToString());
                    }
                    else
                    {
                        lockDrone(hit.collider.gameObject);
                        nbWrongSelection++;

                        FileWriter.WriteScenarioAction(missionDuration, ActionStatu.BAD_SELECTION.ToString());
                    }
                }
                else
                {
                    nbWrongSelection++;

                    FileWriter.WriteScenarioAction(missionDuration, ActionStatu.BAD_SELECTION.ToString());
                }
            }
        }

    }

    //------------select the next ready drone-----------------------

    private void SelectNextReadyDrone()
    {
        int nbPossibleDroneToSelect;
        int chosedDrone;
        GameObject[] availableDrones;

        int i = 0;
        
        availableDrones = getAvailableDrones();

        if (!IsADroneReady(availableDrones))
        {

            Debug.Log("LENGTH = " + availableDrones.Length + " RETOUR NB = " + nbAvailableDrones().ToString());

            // nb = availableDrones.Length;
            //nbPossibleDroneToSelect = nbAvailableDrones();
            nbPossibleDroneToSelect = nbAvailableDrones();

            chosedDrone = Random.Range(0, nbPossibleDroneToSelect);

            //------------------------------------------------------

            availableDrones[chosedDrone].GetComponent<DroneAI>()._currentStatus = DroneStatus.Ready;

            availableDrones[chosedDrone].GetComponent<Renderer>().material.color = new Color(255, 184, 0, 255); //Orange

            droneSelected = availableDrones[chosedDrone];

            haveSelection = true;
            needSelection = false;

            nbReadyAndAvailable--;
            
            timerLockDrone = stapeDuration;
        }
    }


   /* private void SelectNextReadyDrone()
    {
        int nbPossibleDroneToSelect;
        int chosedDrone;
        GameObject[] missionDrones;

        int i=0;

        missionDrones = getMissionDrone();

       if (!IsADroneReady(missionDrones))
        {

            Debug.Log("LENGTH = " + missionDrones.Length + " RETOUR NB = " + nbAvailableDrones().ToString());
            
            // nb = availableDrones.Length;
            //nbPossibleDroneToSelect = nbAvailableDrones();
            nbPossibleDroneToSelect = getMissionDrone().Length;
            
            chosedDrone = Random.Range(0, nbPossibleDroneToSelect);

            //------------------------------------------------------

            //----------ICI-----------------------------
            //if (availableDrones[chosedDrone].GetComponent<DroneAI>()._currentFunction == DroneFunction.onMission)
            {
                missionDrones[chosedDrone].GetComponent<DroneAI>()._currentStatus = DroneStatus.Ready;

                missionDrones[chosedDrone].GetComponent<Renderer>().material.color = new Color(255, 184, 0, 255); //Orange

                droneSelected = missionDrones[chosedDrone];

                haveSelection = true;
                needSelection = false;

                nbReadyAndAvailable--;

                timerLockDrone = stapeDuration;
            }
       }
    }*/
    //-------------end selection of the next ready drone------

    //------------Treate the drone----------------------------
    private void TreateDrone(RaycastHit hit)
    {

        Color color = new Color(0f, 1f, 0f, 1f); //  vert 
        
        hit.collider.GetComponent<Renderer>().material.color = color;
        hit.collider.GetComponent<DroneAI>()._currentStatus = DroneStatus.Treated;
        timerColoredTreatedDrone = timeToMaintainColor;

       // nbReadyAndAvailable--;

        timerSelectNextDrone = timeBetweenStatus;


        haveSelection = false;

        if (/*nbAvailableDrones()*/ nbReadyAndAvailable > 0)
        {
            needSelection = true;
        }
    }
    //-------------end drone treatment----------------------

    //------------Lock the drone----------------------------
    private void lockDrone(GameObject droneToLock)
    {
         Color color = new Color(1f, 0f, 0f, 1f); //  rouge 
                                                 
        droneToLock.GetComponent<Renderer>().material.color = color;
        droneToLock.GetComponent<DroneAI>()._currentStatus = DroneStatus.Locked;
        timerColoredLockDrone = timeToMaintainColor;
                
        //nbReadyAndAvailable = (nbReadyAndAvailable > 0) ? nbReadyAndAvailable - 1 : 0;

        //timerSelectNextDrone = timeBetweenStatus;    // -------ICI  temps selection : retrait-------------

       // isLockedDrone = true;

        haveSelection = false;

        if (/*nbAvailableDrones()*/ nbReadyAndAvailable > 0)
        {
            needSelection = true;
            //nbReadyAndAvailable--;
        }

        nblockedDrones++;
        Debug.Log("nbReadyAndAvailable =  " + nbReadyAndAvailable);
        Debug.Log("needSelection =  " + needSelection);

    }
    //-------------end drone locke------------------------------

    //------------Change scale of the drone when having head pointed on it--------------
    private void changeDroneScaleWithHead()
    {
        GameObject head;
        Vector3 headPosition;
        Vector3 headDirection;

        RaycastHit hit;
        bool hitSometing;

        head = GameObject.Find("HeadDirection");
        headPosition = head.transform.position;
        headDirection = head.transform.up;

        Ray ray = new Ray(headPosition, headDirection);
        hitSometing = Physics.Raycast(ray, out hit, rayLength);
        if (hitSometing)
        { 
            //if a drone no longer has the direction of the head, reset its scale---------
        if (previousHit !=null && previousHit != hit.collider.gameObject)
        {
            if (previousHit.tag == "Drone")
            {
                previousHit.GetComponent<DroneAI>().OnMouseExit();
            }

            previousHit = hit.collider.gameObject;
        }

        //define the current hit-----------------------------------------------------
        previousHit = hit.collider.gameObject;   //-----ICI gestion pb Null-----

        //change the scale of the new hited drone----------------------
        //if (hitSometing)
        //{
            
            if (hit.collider.tag == "Drone")
            {
                hit.collider.gameObject.GetComponent<DroneAI>().OnMouseOver();
            }
        }
    }

    //-------------end head pointed on droge------------------------------

    //------------Change scale of the drone when having head around it-----
    private void changeDroneScaleWithHeadinArea()
    {
        GameObject head;
        Vector3 headPosition;
        Vector3 headDirection;

        head = GameObject.Find("HeadDirection");
        headPosition = head.transform.position;
        headDirection = head.transform.up;

        Ray ray = new Ray(headPosition, headDirection);
        for(int i=0; i<allDrones.Length; i++)
        {
            allDrones[i].GetComponent<DroneAI>().grossirDrone(ray);
        }
     
    }

    //-------------end head pointed aron on droge------------------------------

    //------------test if it is a readied drone --------------------------
    private bool IsADroneReady(GameObject[] availableDrones)
    {
       for (int i = 0; i < availableDrones.Length; i++)
        {
            if (availableDrones[i].GetComponent<DroneAI>()._currentStatus == DroneStatus.Ready)
            {
                return true;
            }
        }

        return false;
    }
    //-------------end test ready--------------------------------------

    //-------------Anciennes version---------------------------------------------------------
    //private GameObject[] getAvailableDrones()
    //{
    //    int j = 0;

    //    GameObject[] drones = new GameObject[nbAvailableDrones()];

    //    for (int i = 0; i < allDrones.Length; i++)
    //    {
    //        if (allDrones[i].GetComponent<DroneAI>()._currentStatus == DroneStatus.Available)
    //        {
    //            drones[j] = allDrones[i];
    //            j++;
    //        }
    //    }

    //    return drones;
    //}

    //private int nbAvailableDrones()
    //{
    //    int nbAvailableDrone = 0;

    //    for (int i = 0; i < allDrones.Length; i++)
    //    {
    //        if (allDrones[i].GetComponent<DroneAI>()._currentStatus == DroneStatus.Available)
    //        {
    //            nbAvailableDrone++;
    //        }
    //    }

    //    return nbAvailableDrone;
    //}

    //-----------------------------------------------------------------------------------

    private GameObject[] getAvailableDrones()
    {
       return allDrones;
    }

    
    private int nbAvailableDrones()
    {
       return allDrones.Length;
    }


    private GameObject[] getReadiedDrones()
    {
        int j = 0;

        GameObject[] drones = new GameObject[nbAvailableDrones()];

        for (int i = 0; i < allDrones.Length; i++)
        {
            if (allDrones[i].GetComponent<DroneAI>()._currentStatus == DroneStatus.Ready)
            {
                drones[j] = allDrones[i];
                j++;
            }
        }

        return drones;
    }

    private GameObject getReadiedDrone()
    {
        GameObject drone=null;

        for (int i = 0; i < allDrones.Length; i++)
        {
            if (allDrones[i].GetComponent<DroneAI>()._currentStatus == DroneStatus.Ready)
            {
                drone = allDrones[i];                
            }
        }

        return drone;
    }


    private GameObject getTreatedDrone()
    {
        GameObject drone = null;

        for (int i = 0; i < allDrones.Length; i++)
        {
            if (allDrones[i].GetComponent<DroneAI>()._currentStatus == DroneStatus.Treated)
            {
                drone = allDrones[i];
            }
        }

        return drone;
    }


    private GameObject getLockedDrone()
    {
        GameObject drone = null;

        for (int i = 0; i < allDrones.Length; i++)
        {
            if (allDrones[i].GetComponent<DroneAI>()._currentStatus == DroneStatus.Locked)
            {
                drone = allDrones[i];
            }
        }

        return drone;
    }


    //-------ICI new---------------------
   private GameObject[] getMissionDrone()
    {
        int j = 0;

        GameObject[] drones = new GameObject[nbReadyAndAvailable];

        for (int i = 0; i < allDrones.Length; i++)
        {
            if (allDrones[i].GetComponent<DroneAI>()._currentFunction == DroneFunction.onMission)
            {
                drones[j] = allDrones[i];
                j++;
            }
        }

        return drones;
    }

    //-----------------------------------
    

    private bool isSelectedDrone()
    {
        return (getReadiedDrone() != null || getTreatedDrone() != null || getLockedDrone() != null);
    }


    private void droneDescription()
    {
        for (int i = 0; i < allDrones.Length; i++)
        {
            Debug.Log("Nom     : " + allDrones[i].name);
            Debug.Log("Couleur : " + allDrones[i].GetComponent<Renderer>().material.color);
            Debug.Log("Statut : " + allDrones[i].GetComponent<DroneAI>()._currentStatus);
            Debug.Log("------------------------------------");
        }
    }


    private void endMission()
    {
        //string missionState;
        //missionState = (isLockedDrone == true) ? "ECHEC || " + nblockedDrones + " ETAPE(S) ECHOUEE(S)" : "SUCCES";
        //missionState = (isLockedDrone == true) ? "ECHEC || " + (numberDronesToTreat-nbRightSelection) + " ETAPE(S) ECHOUEE(S)" : "SUCCES";
        missionState = (isLockedDrone == true) ? "ECHEC" : "SUCCES";

        Debug.Log("MISSION TERMINEE === " + missionState + "===");
        Debug.Log("DUREE DE LA MISSION ## " + missionDuration + "##");

        Debug.Log("Total sélections = " + nbAllSelection);
        Debug.Log("Bonnes sélections = " + nbRightSelection);
        Debug.Log("Mauvaise sélections = " + nbWrongSelection);

        missionEnded = true;
        DroneAI.missionEnded = true;

        GameObject.FindGameObjectWithTag("Table").GetComponent<Renderer>().material.color = Color.grey;
       // GameObject.FindGameObjectWithTag("Table").GetComponent<Renderer>().material.color = new Color(1,1,1,1);

        for (int i = 0; i < allDrones.Length; i++)
        {
            allDrones[i].GetComponent<DroneAI>().resetScale();
        }

    }
}

////gestion du clique sur un drone---------
//private void clickedOnDrone()
//{

//    float r = Random.Range(0f, 1f);
//    float g = Random.Range(0f, 1f);
//    float b = Random.Range(0f, 1f);

//    if (Input.GetMouseButtonDown(0))
//    {
//        RaycastHit hit;
//        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
//        // if (Physics.Raycast(ray, out hit, rayLength, layerMask)
//        if (Physics.Raycast(ray, out hit, rayLength))
//        {
//            if (hit.collider.tag == "Drone" && hit.collider.GetComponent<DroneAI>()._currentStatus == DroneStatus.Available)
//            {
//                Debug.Log(hit.collider.name);
//                Debug.Log(hit.collider.GetComponent<DroneAI>()._currentStatus);

//                Color _color = new Color(0f, 1f, 0f, 1f); // rouge, vert, bleu, opacité
//                hit.collider.GetComponent<Renderer>().material.color = _color;

//                hit.collider.GetComponent<DroneAI>()._currentStatus = DroneStatus.Treated;

//                //-------------------Pour les tests------------------
//                allDrones = GameObject.FindGameObjectsWithTag("Drone");
//                droneDescription();
//                Debug.Log("Drones   : " + allDrones.Length.ToString());
//                Debug.Log("Drones Traités   : " + (allDrones.Length - nbAvailableDrones()).ToString());
//                Debug.Log("Drones disponibles   : " + nbAvailableDrones().ToString());

//                //---------------------------------------------------


//            }
//        }
//    }



//    // Color randomColor = new Color(r, g, b, 1f); // rouge, vert, bleu, opacité
//    // GetComponent<Renderer>().material.color = randomColor;

//}