﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using System.IO;

public class FileWriter
{
    
    private static StreamWriter _SummarizeWriter;
    private static StreamWriter _actionWriter;
        
    public static void initActionWriter(string directory, string scenario)
    {
        //string user = directory + "_" + System.DateTime.Now.Day + "-" + System.DateTime.Now.Month + "-" + System.DateTime.Now.Year + "_" + System.DateTime.Now.Hour + "-" + System.DateTime.Now.Minute + "-" + System.DateTime.Now.Second + "_";
        //string user = scenario + "_" + System.DateTime.Now.Day + "-" + System.DateTime.Now.Month + "-" + System.DateTime.Now.Year + "_" + System.DateTime.Now.Hour + "-" + System.DateTime.Now.Minute + "-" + System.DateTime.Now.Second + "_";
        string user = scenario + "_" + System.DateTime.Now.Day + "-" + System.DateTime.Now.Month + "-" + System.DateTime.Now.Year + "_" + System.DateTime.Now.Hour + "-" + System.DateTime.Now.Minute + "-" + System.DateTime.Now.Second + "_";
        _actionWriter = new StreamWriter(Path.Combine(directory, user + "_log.csv"), true);
        _actionWriter.WriteLine("Time;" + "Action;");
        _actionWriter.Flush();
    }

    public static void WriteScenarioAction(float time, string action)
    {
        if (_actionWriter == null)
        {
            return;
        }
        _actionWriter.WriteLine(time.ToString() + ";" + action + ";");
        _actionWriter.Flush();
    }

    public static void CloseActionWriter()
    {
        if (_actionWriter != null)
            _actionWriter.Close();
    }

    public static void initSummarizeWriter(string directory, string expe)
    {

        //bool exist;
        string user = expe + "_" + System.DateTime.Now.Day + "-" + System.DateTime.Now.Month + "-" + System.DateTime.Now.Year + "_";

        if (!File.Exists(Path.Combine(directory, user + "_Summarize_log.csv")))
        {
            _SummarizeWriter = new StreamWriter(Path.Combine(directory, user + "_Summarize_log.csv"), true);
            _SummarizeWriter.WriteLine("Scenario;" + "Total Objects;" + "number cibles;" + "Start;" + "End;" + "Duration;" + "Selections;" + "Good;" + "Bad;" + "Mission Status;");
        }
        else
        {
            _SummarizeWriter = new StreamWriter(Path.Combine(directory, user + "_Summarize_log.csv"), true);
        }

        //string user = directory + "_" + System.DateTime.Now.Day + "-" + System.DateTime.Now.Month + "-" + System.DateTime.Now.Year + "_" + System.DateTime.Now.Hour + "-" + System.DateTime.Now.Minute + "-" + System.DateTime.Now.Second + "_";
        //string user = expe + "_" + System.DateTime.Now.Day + "-" + System.DateTime.Now.Month + "-" + System.DateTime.Now.Year + "_";
        //_SummarizeWriter.WriteLine("Scenario;" + "Start;" + "End;" + "Duration;" + "Selections;" + "Good;" + "Bad;" + "Mission Status;");
        _SummarizeWriter.Flush();
    }

    public static void WriteMissionSummarize(string missionSummarize)
    {
        if (_SummarizeWriter == null)
        {
            return;
        }
        _SummarizeWriter.WriteLine(missionSummarize);
        _SummarizeWriter.Flush();
    }

    public static void CloseMissionSummarizeWriter()
    {
        if (_SummarizeWriter != null)
            _SummarizeWriter.Close();
    }
}



