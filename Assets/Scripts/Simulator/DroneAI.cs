﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using System.Linq;

public class DroneAI : MonoBehaviour
{
    public DroneStatus _currentStatus { get; set; }
    //---------ICI NEW----------------------
    public float speed { get; set; }
    public DroneFunction _currentFunction { get; set; }
    //--------------------------------------------


    // public static float speed=2.0f;  //---------ICI NEW-------

    public static float droneScale=1.0f;
    public static bool missionEnded;
    public static ExperimentationTypes expeType;


    // public float droneScale { get; set; }


    private Vector3 initialScale;
    private bool isScaleChanged;

    private bool gotScale;


    //public Team Team => _team;

    public Team Team
    {
        get
        {
            return _team;
        }
    }

    [SerializeField] private Team _team;
    [SerializeField] private LayerMask _layerMask;

    private float _attackRange = 2f;
    private float _rayDistance = 2.0f;
    private float _stoppingDistance = 1.5f;

    private Vector3 _destination;
    private Quaternion _desiredRotation;
    private Vector3 _direction;

    private Vector3 _rightDdirection;
    private Vector3 _leftDirection;

    private DroneAI _target;
    private DroneState _currentState;


    //-------------ICI --------------------------
    private DroneSituation _currentSituation;  
    

    // Start is called before the first frame update
    public void Start()
    {
        _currentStatus = DroneStatus.Available;

        //-------------------ICI----------------
        _currentSituation = DroneSituation.outArea;
        isScaleChanged = false;
        gotScale = false;
        missionEnded = false;
    }
    

    // Update is called once per frame
    private void Update()
    {
       if (gotScale == false)
       {
           transform.localScale = transform.localScale * droneScale;
           initialScale = transform.localScale;
           gotScale = true;
       }

        //Debug.Log(transform.localScale);

        StateManager();

        //if (expeType == ExperimentationTypes.MouseAndHighlight || expeType == ExperimentationTypes.TouchAndHead)
        if (expeType == ExperimentationTypes.MouseAndHighlight)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            grossirDrone(ray);
        }

    }

    //manage Drone state
    private void StateManager()
    {
        switch (_currentState)
        {
            case DroneState.Wander:
                {
                    if (NeedsDestination())
                    {
                        GetDestination();
                    }

                    transform.rotation = _desiredRotation;

                    transform.Translate(Vector3.forward * Time.deltaTime * speed);

                    var rayColor = IsPathBlocked() ? Color.red : Color.green;
                    Debug.DrawRay(transform.position, _direction * _rayDistance, rayColor);

                    Debug.DrawRay(transform.position, transform.right * _rayDistance, Color.red);

                    Debug.DrawRay(transform.position, -transform.right * _rayDistance, Color.yellow);
                    

                    while (IsPathBlocked())
                    {
                       // Debug.Log("Path Blocked in Fornt");
                        GetDestination();
                    }

                   var targetToAggro = CheckForAggro();
                    if (targetToAggro != null)
                    {
                        _target = targetToAggro.GetComponent<DroneAI>();
                        _currentState = DroneState.Chase;
                    }

                    break;
                }
            case DroneState.Chase:
                {
                    if (_target == null)
                    {
                        _currentState = DroneState.Wander;
                        return;
                    }

                    transform.LookAt(_target.transform);
                    transform.Translate(Vector3.forward * Time.deltaTime * speed);

                    if (Vector3.Distance(transform.position, _target.transform.position) < _attackRange)
                    {
                        _currentState = DroneState.Attack;
                    }
                    break;
                }
            case DroneState.Attack:
                {
                    if (_target != null)
                    {
                        Destroy(_target.gameObject);
                    }

                    // play laser beam

                    _currentState = DroneState.Wander;
                    break;
                }
        }
    }
    
    private bool IsPathBlocked()
    {
        Ray ray = new Ray(transform.position, _direction);
        var hitSomething = Physics.RaycastAll(ray, _rayDistance, _layerMask);
        return hitSomething.Any();
    }

    private bool IsPathOfRightBlocked()
    {
        Ray ray = new Ray(transform.position, transform.right);
        var hitSomething = Physics.RaycastAll(ray, _rayDistance, _layerMask);
        return hitSomething.Any();
    }

    private void GetDestination()
    {
        Vector3 testPosition = (transform.position + (transform.forward * 4f)) +
                               new Vector3(UnityEngine.Random.Range(-4.5f, 4.5f), 0f,
                                   UnityEngine.Random.Range(-4.5f, 4.5f));

        _destination = new Vector3(testPosition.x, 1f, testPosition.z);

        _direction = Vector3.Normalize(_destination - transform.position);
        _direction = new Vector3(_direction.x, 0f, _direction.z);
        _desiredRotation = Quaternion.LookRotation(_direction);
    }

    private bool NeedsDestination()
    {
        if (_destination == Vector3.zero)
            return true;

        var distance = Vector3.Distance(transform.position, _destination);
        if (distance <= _stoppingDistance)
        {
            return true;
        }

        return false;
    }

    Quaternion startingAngle = Quaternion.AngleAxis(-60, Vector3.up);
    Quaternion stepAngle = Quaternion.AngleAxis(5, Vector3.up);

    private Transform CheckForAggro()
    {
        float aggroRadius = 5f;

        RaycastHit hit;
        var angle = transform.rotation * startingAngle;
        var direction = angle * Vector3.forward;
        var pos = transform.position;
        for (var i = 0; i < 0; i++)
        {
            if (Physics.Raycast(pos, direction, out hit, aggroRadius))
            {
                var drone = hit.collider.GetComponent<DroneAI>();
                if (drone != null && drone.Team != gameObject.GetComponent<DroneAI>().Team)
                {
                    Debug.DrawRay(pos, direction * hit.distance, Color.red);
                    return drone.transform;
                }
                else
                {
                    Debug.DrawRay(pos, direction * hit.distance, Color.yellow);
                }
            }
            else
            {
                Debug.DrawRay(pos, direction * aggroRadius, Color.white);
            }
            direction = stepAngle * direction;
        }

        return null;
    }


    //-----------------ICI NEW---------------------------------------------------------------
    private void grossirDrone1()
    {
        float aggroRadius = 5f;

        RaycastHit hit;
        var angle = transform.rotation * startingAngle;
        var direction = angle * Vector3.forward;
        var pos = transform.position;
        for (var i = 0; i < 4; i++)
        {
            if (Physics.Raycast(pos, direction, out hit, aggroRadius))
            {
                if (hit.collider.tag == "Drone")
                {

                }
            }

        }
    }

   /* public void grossirDrone()
    {
        Vector3 dronePosition;
        Vector3 MousePosition;

        float distance = 0;

        //  if (Input.GetMouseButtonDown(0))
        if (missionEnded == false)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 5000))
            {
                Debug.ClearDeveloperConsole();
                dronePosition = transform.position;
                Debug.Log("Drone = " + dronePosition);

                MousePosition = hit.point;
                Debug.Log("Souris = " + MousePosition);

                distance = Vector3.Distance(MousePosition, dronePosition);
                Debug.Log("Distance = " + distance);

                if (_currentSituation != DroneSituation.Pointed)
                {
                    if (distance < 4)
                    {
                        changeScale(1.35f);
                        _currentSituation = DroneSituation.inArea;
                    }
                    else
                    {
                        resetScale();
                    }
                }
            }
        }
        else
        {
            resetScale();
        }

    }*/

    public void grossirDrone(Ray ray)
    {
        Vector3 dronePosition;
        Vector3 MousePosition;

        float distance = 0;

        //  if (Input.GetMouseButtonDown(0))
        if (missionEnded == false)
        {
            RaycastHit hit;
            //Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 5000))
            {
                //Debug.ClearDeveloperConsole();
                dronePosition = transform.position;
                //Debug.Log("Drone = " + dronePosition);

                MousePosition = hit.point;
                //Debug.Log("Souris = " + MousePosition);

                distance = Vector3.Distance(MousePosition, dronePosition);
                //Debug.Log("Distance = " + distance);

                if (_currentSituation != DroneSituation.Pointed)
                {
                    if (distance < 4)
                    {
                        changeScale(1.25f);
                        _currentSituation = DroneSituation.inArea;
                    }
                    else
                    {
                        resetScale();
                    }
                }
            }
        }
        else
        {
            resetScale();
        }

    }

    //------------------------------------------------------------------------------
    
    //manage Drone status
    private void StatusManager()
    {

    }

    //-----------manage drone scale when mouse over the drone------------------
    public void OnMouseOver()
    {
        if (missionEnded == false)
        {
            if (expeType == ExperimentationTypes.MouseAndHighlight || expeType == ExperimentationTypes.TouchAndHead)
            {
                if (_currentStatus == DroneStatus.Available || _currentStatus == DroneStatus.Ready)
                {
                    resetScale();
                    changeScale(1.5f);
                    _currentSituation = DroneSituation.Pointed;
                    //changeScale();
                }
            }
        }

    }

    public void OnMouseExit()
    {
        if (isScaleChanged == true)
        {
            resetScale();
        }
    }


    //private void OnCollisionEnter(Collision collision)
    //{
    //    if(collision.gameObject.name=="aName")
    //    {

    //    }
    //}

    private void changeScale()
    {
        // if (transform.localScale.x < 4.5)
       // Debug.Log("Distance = " + Vector3.Distance(transform.localScale, new Vector3(3.0f, 1.0f, 8.0f)));

       //if (Vector3.Distance(transform.localScale,new Vector3(3.0f, 1.0f, 8.0f))<0)
       if (transform.localScale.x < droneScale + 0.5f)
        {
            transform.localScale = transform.localScale + new Vector3(0.5f, 0.0f, 0.75f);
            isScaleChanged = true;
        }
    }

    //-------------------------------ICI NEW-------------------------------------
    private void changeScale(float s)
    {
        if (transform.localScale.x < droneScale * s)
        {
            transform.localScale = transform.localScale * s;
            isScaleChanged = true;
        }
    }
    //---------------------------------------------------------------------------
    public void resetScale()
    {
        if (isScaleChanged == true)
        {
            transform.localScale = initialScale;
            isScaleChanged = false;
            _currentSituation = DroneSituation.outArea;
        }
            
    }

}

// Data Structures------------------

//public enum Team
//{
//    Red,
//    Blue
//}

//public enum DroneState
//{
//    Wander,
//    Chase,
//    Attack
//}

//public enum DroneStatus
//{
//    Available,
//    Ready,
//    Treated,
//    Locked,
//    Completed
//}

//public struct TestItem
//{
//    public int id { get; set; }
//    public string text { get; set; }

//}

//public enum ExperimentationTypes
//{
//    Mouse = 0,
//    MouseAndhHighlight,
//    Touch,
//    TouchAndhHead
//}


