﻿using System;
using UnityEngine;

namespace TobiiGlasses
{
    public class ConvertGP3Data
    {


        // Class GazePosition3D
        public class Data { }

        //classe qui fabrique les positions du regard
        public class GazePosition3D : Data
        {
            public string ts { get; set; }
            public string gp3 { get; set; }
            public string s { get; set; }

            public GazePosition3D(JSONObject jo)
            {
                this.gp3 = jo.GetField("gp3").ToString();
                this.ts = jo.GetField("ts").ToString();
                this.s = jo.GetField("s").ToString();
            }
        }

        public static float[] CData(string dataReceivedString)
        {
            JSONObject jobject = new JSONObject(dataReceivedString);
            GazePosition3D gazePosition3D = new GazePosition3D(jobject);

            /*-------------------Debut ajout-----------------
            Debug.Log(jobject.str);
            Debug.Log("Valeru  de TS= " + gazePosition3D.ts);
            Debug.Log("Valeru  de GP3= " + gazePosition3D.gp3);
            Debug.Log("Valeru  de S= " + gazePosition3D.s);
            //--------------------------------------------------*/
           // Debug.Log(gazePosition3D.gp3);

            String coordonnee = gazePosition3D.gp3.Replace("[", "").Replace("]", "").Replace(" ", "");
            string[] stringSeparators = new string[] { "," };
            string[] result = coordonnee.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
            string xString = (string)result.GetValue(0);
            string yString = (string)result.GetValue(1);
            string zString = (string)result.GetValue(2);
            float x = Convert.ToSingle(xString);
            float y = Convert.ToSingle(yString);
            float z = Convert.ToSingle(zString);
            //float x = float.Parse("0,0");
            //Console.WriteLine(x);
            //Console.WriteLine("Detecte Gaze Position 3D " + gp3D + "x"+ xString + "\ny"+ yString + "\nz"+ zString);
            float[] output = new float[3];
            output[0] = x;
            output[1] = y;
            output[2] = z;
            return output;
        }
    }
}
