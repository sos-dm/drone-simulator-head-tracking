﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;

public class RightLaserBehaviour : MonoBehaviour
{

	private Color enabledColor = new Color (70f/255f, 70f/255f, 70f/255f);
	private Color disabledColor = new Color(170f/255f, 170f/255f, 170f/255f);

	private float thickness = 0.001f;
	private float infiniteLength = 100f;
	//private int layer = 9; // layer 9 is for focusable selectors

	private GameObject pointer;
	private LineRenderer pointerRenderer;

	public void Start()
    {

		// Setup laser as a LineRenderer
		pointer = new GameObject ("Pointer");
		pointer.transform.parent = this.transform;
		pointerRenderer = pointer.AddComponent<LineRenderer> ();
		pointerRenderer.material = new Material(Shader.Find("Standard"));
		pointerRenderer.material.color = disabledColor;

		setPointerTransform (infiniteLength);
	}

	void Update()
    {
			Ray raycast = new Ray (transform.position, transform.forward);
			RaycastHit hitObject;
        //int layerMask = 1 << layer;
        bool rayHit = Physics.Raycast(raycast, out hitObject, infiniteLength);//, layerMask);

			setEnabled (rayHit);

			float length = infiniteLength;
			if (rayHit) 
				length = hitObject.distance;
			
			setPointerTransform (length);

			if (rayHit) {
				
			} else {
				
			}
	}

	private void setEnabled(bool value) {
		if (value) {
			pointerRenderer.material.color = enabledColor;
		} else {
			pointerRenderer.material.color = disabledColor;
		}
	}

	private void setPointerTransform(float setLength) {
		pointerRenderer.startWidth = thickness;
		pointerRenderer.endWidth = thickness;
		pointerRenderer.SetPosition (0, transform.position);
		pointerRenderer.SetPosition (1, transform.position + (transform.forward * setLength));
	}
}