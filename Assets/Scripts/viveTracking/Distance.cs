﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Distance : MonoBehaviour
{
    [Tooltip("objet dont la distance par rapport à l'objet courant sera donnée")]
   // public GameObject Objet;
    //public static float Distance_;

    // pour la distance au centre de l'écran
    public GameObject txtDistance;
    public GameObject txtEcran;
    public GameObject ecran;

    float distance;
    float largeurE;
    float hauteurE;
    float angleCam;

    // Start is called before the first frame update
    void Start()
    {
        //largeurE = Screen.width * ((Screen.dpi) * (210f/2480)/300f);

        largeurE = Screen.width / Screen.dpi * 2.54f;
        hauteurE = Screen.height / Screen.dpi * 2.54f;

        //largeurE = Screen.width / 36.7953f;
        //hauteurE = Screen.height / 37.7953f;
        angleCam = Camera.main.fieldOfView;

        //redimensionner l'objet screen à la taille de l'écran
       // ecran.transform.localScale.x = largeurE * 10f;
        
    }

    // Update is called once per frame
    void Update()
    {

        largeurE = Screen.width / Screen.dpi * 2.54f;
        hauteurE = Screen.height / Screen.dpi * 2.54f;

        //Distance_ = Vector3.Distance(transform.position, Objet.transform.position);
        // Debug.Log("Distace de "+gameObject.name + " au centre = " + Distance_);
        Text txtDist = txtDistance.GetComponent<Text>();
        string texteDist;

        Text txtEcr = txtEcran.GetComponent<Text>();
        string texteEcran;

        distance = (largeurE / 2 * Mathf.Tan(angleCam * Mathf.Deg2Rad))*0.01f;
        //distance = largeurE / 2 * Mathf.Tan(angleCam * 2 * Mathf.PI / 360);

        texteDist = "\r\nDistance = " + distance.ToString() + " m";
        txtDist.text = texteDist;

        texteEcran = "Largeur = " + largeurE.ToString() + " cm\r\n" + "Hauteur = " + hauteurE.ToString() + " cm\r\n";
        texteEcran = "Ecran : \r\n" + texteEcran;
        txtEcr.text = texteEcran;

    }

    
}
