﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Valve.VR;

public class RayCasts : MonoBehaviour
{
    public GameObject laserOrigin;
    public GameObject laserEnd;
    public GameObject ecran;

    private Color enabledColor = new Color(70f / 255f, 70f / 255f, 70f / 255f);
    private Color disabledColor = new Color(170f / 255f, 170f / 255f, 170f / 255f);

    private float thickness = 0.006f;
    private float laserLength = 0f;
    private float rayLength = 0f;


    private GameObject defaultLaserEnd;
    private LineRenderer laserLine;

    private GameObject cam;
    public bool isManette;
    private Vector3 direction;
    

    public void Start()
    {
       // définir le rayon comme étant une ligne
       // laserOrigin = new GameObject("OrigineLaser");
       // laserOrigin.transform.parent = this.transform;
        laserLine = laserOrigin.AddComponent<LineRenderer>();

        if(laserEnd==null)
        {
            defaultLaserEnd = new GameObject("DefaultLaserEnd");
            defaultLaserEnd.AddComponent<Rigidbody>();
            defaultLaserEnd.AddComponent<BoxCollider>();
            defaultLaserEnd.AddComponent<MeshRenderer>();

            defaultLaserEnd.transform.position = new Vector3(0, 0, 3);
            laserEnd = defaultLaserEnd;
        }

        // laserLine.material = new Material(Shader.Find("Standard"));

        // Debug.Log(transform.name + " BOOL= " + isManette.ToString() + " ORIGINE= " + laserOrigin.ToString() + " END = " + laserEnd.ToString());
        rayLength = Vector3.Distance(laserOrigin.transform.position, laserEnd.transform.position);

        if (isManette)
        {
            laserLine.material.color = Color.green;
            laserLength=rayLength;
        }
            
        else
        {
            laserLine.material.color = Color.cyan;
            
        }
                    
        setPointerTransform(rayLength);

        cam = GameObject.Find("Main Camera");
        
    }
    

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        Quaternion q;
        Vector3 eulerAngle;

        /*
        // gestion de plusieurs objets touchés--------------------------
        RaycastHit[] hits;
        hits = Physics.RaycastAll(transform.position, transform.up, 10f);
        Debug.Log("il y a "+ hits.Length + " objet(s) touché(s).");
        */

        //direction = laserEnd.transform.position - laserOrigin.transform.position;

        Debug.DrawRay(laserOrigin.transform.position, laserEnd.transform.forward * /*direction*/ laserLength, Color.red);
        if (Physics.Raycast(laserOrigin.transform.position, laserEnd.transform.forward /*direction*/, out hit, laserLength))
        {
            
            Debug.Log("L'objet " + hit.transform.name + " a été touché");

            if (hit.collider && ecran != null && hit.transform.name != ecran.transform.name)
            {
                Color color = new Color(0f, 0f, 1f, 1f); // bleu
                hit.collider.GetComponent<Renderer>().material.color = laserLine.GetComponent<LineRenderer>().material.color;
            
            }
            
        }
        
        // Ray raycast = new Ray(transform.position, transform.forward);
        // RaycastHit hitObject;
        //int layerMask = 1 << layer;
        // bool rayHit = Physics.Raycast(raycast, out hitObject, laserLength);//, layerMask);

        // setEnabled(rayHit);

        float length = 0f;
        //if (rayHit)
        //length = hitObject.distance;

        // length = Vector3.Distance(origineLaser.transform.position,extremiteLaser.transform.position);

        //length = Vector3.Distance(laserOrigin.transform.position, laserEnd.transform.position);
        //setPointerTransform(length);

        setPointerTransform(rayLength); 
        /* if (rayHit)
         {

         }
         else
         {

         }*/

        //------------A quoi ressemble le quaternium?----------------
        q = getHeadRotation(cam);
        eulerAngle = q.eulerAngles;
       // Debug.Log("Rotation CAMERA : Quaternion = " + q.ToString("f3") + " Euler Angle = " + eulerAngle.ToString());

        //Debug.Log(getHeadRotationDirection(q));
        
    }

    /* private void setEnabled(bool value)
     {
         if (value)
         {
             laserLine.material.color = enabledColor;
         }
         else
         {
             laserLine.material.color = disabledColor;
         }
     }*/

    private void setPointerTransform(float setLength)
    {
        laserLine.startWidth = thickness;
        laserLine.endWidth = thickness;
        laserLine.SetPosition(0, laserOrigin.transform.position);
        //laserLine.SetPosition(1, laserOrigin.transform.position + (/*laserEnd.transform.forward*/SteamVR_TrackedObject.eulerAngle * setLength));
        laserLine.SetPosition(1, laserEnd.transform.position /*+ (/*laserEnd.transform.forward*/ /*direction * setLength)*/);
    }

    private Quaternion getHeadRotation(GameObject gameObject)
    {

        return gameObject.transform.rotation;
    }

    private string getHeadRotationDirection(Quaternion q)
    {
        string autourDeX="";
        string autourDeY="";
        string rotation="";
        Vector3 v;

        v = q.eulerAngles;

        //--------rotation autour de l'axe X----------------------------
        if (q.x >= 0)
        {
            autourDeX = "le BAS de " + v.x.ToString() + "° ";
        }
        else if (q.x < 0)
        {
            autourDeX = "le HAUT de " + (360f - v.x).ToString() + "° ";
        }
        else
        {

        }

        //--------rotation autour de l'axe Y----------------------------
        if (q.y > 0)
        {
            autourDeY = "la DROITE de " + v.y.ToString() + "° ";
        }
        else if (q.y < 0)
        {
            autourDeY = "la GAUCHE de " + (360f - v.y).ToString() + "° ";
        }
        else
        {

        }

        rotation= "Rotation vers " + autourDeY + " et vers" + autourDeX;

        return rotation;
    }

    //gestion de la rotation du laser en fonction de celle de la tête 
    private void rotatedTheLaser(GameObject laserEnd, GameObject tete)
    {
        laserEnd.transform.rotation=tete.transform.rotation;
    }

}
