﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Oscillator : MonoBehaviour
{
    float timeCounter = 0;

    Texte t = new Texte();
    string data;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timeCounter += Time.deltaTime;
        float x = Mathf.Cos(timeCounter);
        float y = Mathf.Sin(timeCounter);
        float z = 0;

        data = timeCounter.ToString() + " // Cos = " + x + " // Sin = " + y;
        t.ecrireFichier("data.txt", data);



        transform.position = new Vector3(x, y, z);
    }
}
