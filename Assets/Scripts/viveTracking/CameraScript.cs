﻿
using UnityEngine;


// Script for the user camera tied to the OptiTrack target
public class CameraScript : MonoBehaviour {

    public GameObject curseur;

    Camera cam;
    public GameObject window;

    Texte t = new Texte();
    string data;

    // Use this for initialization
    void Start () {
        cam = GetComponent<Camera>();

        //transform.localPosition = new Vector3(0,0,1);
        //cam = Camera.main;
        //window = GameObject.Find("Window");
    }

	// Update is called once per frame
	void Update () {

        // Retrieving screen size in the global system
        float scrWidth = window.transform.localScale.x;
        float scrHeight = window.transform.localScale.y;

		Vector3 scrPos = window.transform.localPosition;
        // Vector3 scrPos = window.transform.position;


       // Vector3 camPos = transform.position;

        Vector3 camPos = curseur.transform.localPosition;

        //Vector3 camPos = transform.parent.TransformPoint(curseur.transform.position);


        // Vector3 camPos = transform.localPosition + transform.parent.position;


        //journaliser-------
        data = "=>     Camera Position : "+camPos.ToString() + " ## Camera Local Position"+ transform.localPosition.ToString() + " ## Parent position : " +  transform.parent.position.ToString();
       // print(data);

        print( camPos - scrPos);

        // t.ecrireFichier("data.txt", data);

        // Compute near clipping plane size (screen projected on the near plane)
        float scrWidthNear = (cam.nearClipPlane / (scrPos.z - camPos.z)) * scrWidth;
        float scrHeightNear = (cam.nearClipPlane / (scrPos.z - camPos.z)) * scrHeight;
        Vector3 scrPosNear = new Vector3(camPos.x + (cam.nearClipPlane / (scrPos.z - camPos.z)) * (scrPos.x - camPos.x),
                                        camPos.y + (cam.nearClipPlane / (scrPos.z - camPos.z)) * (scrPos.y - camPos.y), 
                                        camPos.z + cam.nearClipPlane);

        // Left, right, top and bottom of the screen projection
        float left = scrPosNear.x - (scrWidthNear/2) - camPos.x;
        float right = scrPosNear.x + (scrWidthNear / 2) - camPos.x;
        float top = scrPosNear.y + (scrHeightNear/2) - camPos.y;
        float bottom = scrPosNear.y - (scrHeightNear / 2) - camPos.y;

        // Aspect and field of view do not absolutely need the following changes
        //cam.aspect = scrWidth / 9f;
        //cam.fieldOfView = (Mathf.Atan(top / cam.nearClipPlane) - Mathf.Atan(bottom / cam.nearClipPlane)) * 57.2957795f;

        // Calling method to adjust the projection matrix
        Matrix4x4 m = PerspectiveOffCenter(left, right, bottom, top, cam.nearClipPlane, cam.farClipPlane);

        //cam.projectionMatrix = m;

        // cam.projectionMatrix = transform.parent.worldToLocalMatrix * m;

        cam.projectionMatrix = m;
        cam.transform.localPosition = camPos;

    }

    static Matrix4x4 PerspectiveOffCenter(float left, float right, float bottom, float top, float near, float far)
    {
        float x = 2.0f * near / (right - left);
        float y = 2.0f * near / (top - bottom);
        float a = (right + left) / (right - left);
        float b = (top + bottom) / (top - bottom);
        float c = -(far + near) / (far - near);
        float d = -(2.0f * far * near) / (far - near);
        float e = -1.0f;

        Matrix4x4 m = new Matrix4x4();
        
        m[0, 0] = x; m[0, 1] = 0; m[0, 2] = a; m[0, 3] = 0;
        m[1, 0] = 0; m[1, 1] = y; m[1, 2] = b; m[1, 3] = 0;
        m[2, 0] = 0; m[2, 1] = 0; m[2, 2] = c; m[2, 3] = d;
        m[3, 0] = 0; m[3, 1] = 0; m[3, 2] = e; m[3, 3] = 0;

        return m;
    }
}
