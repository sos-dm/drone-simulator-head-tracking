﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR;

public class ViveInput : MonoBehaviour
{
    //public SteamVR_Action_Single squeezeAction;
    //public SteamVR_Action_Vector2 touchPadAction;
    public SteamVR_Action_Boolean pressPad;
    //public GameObject myText;
    public GameObject centreEcran;
    public GameObject camera;
    public GameObject objetSurLeSol;

    public GameObject controllerLaser;


    // private controleur

    string data;
    float largeurEcran = 0;
    float hauteurEcran = 0;
    float idealDistance = 0;

    Vector3 coordonneeCentre = Vector3.zero;

    Vector3 positionSurSol = Vector3.zero;
    Vector3 positionSurEcran = Vector3.zero;
   

    //Camera c;
    GameObject cam;
   
    //les coins de l'écran

    // private Vector3[] coord { get; set; } = new Vector3[4] ;
    private Vector3[] quatreCoinsEcran = new Vector3[4] ;
    
    Texte t = new Texte();

    private int i = 0;
    

    // Start is called before the first frame update
    void Start()
    {
        cam = GameObject.Find("Curseur");
        // c = new Camera();

        //fixer le centre de l 'écran pour les tests-------------------
        i = 5;
        //coordonneeCentre = new Vector3(0.1f,1.2f,1.7f);
        //coordonneeCentre = new Vector3(0.8f, 1.2f, 1.7f);
        coordonneeCentre = new Vector3(1.1f, 1.3f, 1.9f);

        //-------------------------------------------------------------
    }

    // Update is called once per frame
    void Update()
    {

        positionSurSol = objetSurLeSol.transform.position;
        //print("Position sur Sol : " + objetSurLeSol.transform.position.ToString());
        //print("Position sur Sol Bis : " + positionSurSol.ToString());


        //fixer le centre de l'écran  pour test--------------------------------------------------------------------

        positionSurEcran = -coordonneeCentre;

        if (camera != null)
            camera.transform.localPosition = new Vector3(positionSurEcran.x, positionSurEcran.y, positionSurEcran.z);

        if (controllerLaser != null)
            controllerLaser.transform.localPosition = new Vector3(positionSurEcran.x, positionSurEcran.y, positionSurEcran.z);


        //data = "LOCAL POSITON   " + "=" + camera.transform.localPosition.ToString();
        // print(data);
        //t.ecrireFichier("data.txt", data);

        //--------------------------------------------------------------------------------------------------


        // déterminer les paramètres de l'écran-----------------------------------
        //------------------------------------------------------------------------
        #region Paramètres de lécran

        bool padPressed = pressPad.GetStateDown(SteamVR_Input_Sources.Any);
        if (padPressed && i < 4)
        {
            quatreCoinsEcran[i] = cam.transform.position;
            print("Tactile appuyé");
            t.ecrireFichier("data1.txt", "Tactile appuyé");

           // print("coordonnées de P" + i + "= " + quatreCoinsEcran[i].ToString());
            data = "coordonnées de P" + i + "= " + quatreCoinsEcran[i].ToString();
            t.ecrireFichier("data1.txt", data);
            i++;
        }

        if (i == 4)
        {
            data = "\r\n---------------------------------------------\r\n\n Coordonnées des 4 coins de l'écran --------- \r\n\n---------------------------------------------\r\n\n";
            //print(data);
            t.ecrireFichier("data1.txt", data);

            // les 4 coins de l écran--------------------------------------
            #region Les 4 coins ecrans 
            for (int j = 0; j < 4; j++)
            {
                data = "P" + j + "=" + quatreCoinsEcran[j].ToString();
                //print(data);
                t.ecrireFichier("data1.txt", data);
            }

            #endregion

            //coordonnées du centre de l'écran------------------------------
            #region Coordonnees centre de l'écran
           
            float xc = (quatreCoinsEcran[0].x + quatreCoinsEcran[2].x) / 2;
            float yc = (quatreCoinsEcran[0].y + quatreCoinsEcran[2].y) / 2;
            float zc = (quatreCoinsEcran[0].z + quatreCoinsEcran[2].z) / 2;

            coordonneeCentre = new Vector3(xc, yc, zc);

            data = "\r\n---------------------------------------------\r\n\n Coordonnées du centre de l'écran --------- \r\n\n---------------------------------------------\r\n\n";
            //print(data);
            t.ecrireFichier("data1.txt", data);

            data = "Centre" + "=" + coordonneeCentre.ToString();
           // print(data);
            t.ecrireFichier("data1.txt", data);

            // placer l'objet au centre de l'écran---------------------
            centreEcran.transform.position = coordonneeCentre;


            positionSurEcran =  - coordonneeCentre;

            camera.transform.localPosition = new Vector3(positionSurEcran.x, positionSurEcran.y, positionSurEcran.z);

            data = "LOCAL POSITON   " + "=" + camera.transform.localPosition.ToString();
            // print(data);
            t.ecrireFichier("data1.txt", data);



            #endregion

            //dimensions de l'écran-----------------------------------------
            #region dimension de l'écran

            largeurEcran = Vector3.Distance(quatreCoinsEcran[0], quatreCoinsEcran[3]);
            hauteurEcran = Vector3.Distance(quatreCoinsEcran[0], quatreCoinsEcran[1]);

            data = "\r\n---------------------------------------------\r\n\n Dimensions de l'écran --------- \r\n\n---------------------------------------------\r\n\n";
            //print(data);
            t.ecrireFichier("data1.txt", data);

            data = "Lageur = " + largeurEcran.ToString();
            //print(data);
            t.ecrireFichier("data1.txt", data);

            data = "Hauteur = " + hauteurEcran.ToString();
            //print(data);
            t.ecrireFichier("data1.txt", data);
            #endregion

            // Distance idéale pour la vue----------------------------------
            #region Distance de vue idéale
            
            //idealDistance = largeurEcran / 2 * (Mathf.Tan(c.fieldOfView * Mathf.Deg2Rad));


            /* if (c != null)
             {
                 idealDistance = largeurEcran / 2 * (Mathf.Tan(c.fieldOfView * Mathf.Deg2Rad));
             }
             else
             {
                 idealDistance = largeurEcran / 2 * (Mathf.Tan(60 * Mathf.Deg2Rad));
             }*/



            //Text txt = myText.GetComponent<Text>();

            // txt.text = txt.text+idealDistance.ToString();




            data = "\r\n---------------------------------------------\r\n\n Distance de vue idéale  --------- \r\n\n---------------------------------------------\r\n\n";
            //print(data);
            t.ecrireFichier("data1.txt", data);

            data = "Distance de vue = " + idealDistance.ToString();
            //print(data);
            t.ecrireFichier("data1.txt", data);

            #endregion
            i++;
        }

        #endregion

        #region 
        // pour gérer le changement de repère---------------------
        //-############################################################################--
       /* if (i >= 4)
        {
            positionSurEcran = positionSurSol - coordonneeCentre;

            camera.transform.localPosition = new Vector3(positionSurEcran.x,positionSurEcran.y,positionSurEcran.z);

           // print("Distance Ecran :"+ idealDistance.ToString()+" ## Position Curseur : "+ curseur.transform.position.ToString());

            //cam.transform.position = new Vector3(positionSurEcran.x, positionSurEcran.y, cam.transform.position.z);

            //transform.localPosition = Vector3.SmoothDamp(transform.localPosition, newPosition, ref velocity, smoothTime);

            data = "\r\n****************************************\r\nPosition sur Sol = " + positionSurSol.ToString();
            //print(data);
            t.ecrireFichier("data.txt", data);


            data = "Position sur Ecran = " + positionSurEcran.ToString();
            //print(data);
            t.ecrireFichier("data.txt", data);
        }*/
        //-############################################################################--
        #endregion


        /*if (SteamVR_Actions._default.Teleport.GetStateDown(SteamVR_Input_Sources.Any))
        {
            print("Teleport down");
        }

        if (SteamVR_Actions._default.Teleport.GetStateUp(SteamVR_Input_Sources.Any))
        {
            print("Teleport up");
        }


        if (SteamVR_Actions._default.GrabPinch.GetStateDown(SteamVR_Input_Sources.Any))
        {
            print("Grab pinch down");
        }

        if (SteamVR_Actions._default.GrabPinch.GetStateUp(SteamVR_Input_Sources.Any))
        {
            print("Grab pinch up");
        }

        float triggerValue = squeezeAction.GetAxis(SteamVR_Input_Sources.Any);
        if (triggerValue > 0.0f)
        {
            // print(triggerValue);
        }

        Vector2 touchPasValue = touchPadAction.GetAxis(SteamVR_Input_Sources.Any);
        if (touchPasValue != Vector2.zero)
        {
            // print(touchPasValue);
        }*/


    }

    //private void LateUpdate()
    //{
    //    #region 
    //    // pour gérer le changement de repère---------------------
    //    //-############################################################################--
    //    if (i >= 4)
    //    {
    //        positionDansVirtuel = positionDansReel - coordonneeCentre;

    //        curseur.transform.position = positionDansVirtuel;


    //        print("Cursuer Virtuel : " + positionDansVirtuel.ToString());

    //        // curseur.transform.Translate(positionDansVirtuel);

    //        data = "\r\n****************************************\r\nCurseur dans Reel = " + positionDansReel.ToString();
    //        //print(data);
    //        t.ecrireFichier("data.txt", data);


    //        data = "Curseur dans Virtuel = " + positionDansVirtuel.ToString();
    //        //print(data);
    //        t.ecrireFichier("data.txt", data);
    //    }
    //    //-############################################################################--
    //    #endregion
    //}

    //public static Vector3[] Coordonnees(GameObject gObject, SteamVR_Action_Boolean uneAction)
    //{
    //    //GameObject cam = GameObject.FindGameObjectWithTag("MainCamera");
    //    Vector3[] coord = new Vector3[4];
    //    int i = 0;

    //    bool padPressed = uneAction.GetStateDown(SteamVR_Input_Sources.Any);
    //    if (padPressed && i < 4)
    //    {
    //        coord[i] = gObject.transform.position;
    //        print("Tactile appuyé");
    //        print("coordonnées de P" + i + "= " + coord[i].ToString());
    //        i++;
    //    }

    //    return coord;
    //}

}
