﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.IO;

public class Texte : MonoBehaviour
{
    public GameObject myText;
    GameObject cam ;
    GameObject origine;
    GameObject centreEcran;


    // Start is called before the first frame update
    void Start()
    {
        //cam = GameObject.Find("MainCamera");
        //origine = GameObject.Find("Origine");
        centreEcran = GameObject.Find("CentreEcran");
    }
    
    // Update is called once per frame
    void Update()
    {
        float distance;
        // Distance d = new Distance();
        Text txt = myText.GetComponent<Text>();
        string texte;
        string rtx = transform.rotation.x.ToString();
        string rty = transform.rotation.y.ToString();
        string rtz = transform.rotation.z.ToString();

        distance= Vector3.Distance(transform.position, centreEcran.transform.position);
       // distance = Vector3.Distance(transform.position, cam.transform.position);


        //texte = name.ToUpper().ToString() + " " + Distance.Distance_.ToString()+" ----------\n";

        texte = name.ToUpper().ToString() + " - " + centreEcran.name.ToUpper().ToString() + " : " + distance.ToString()+" \n";

        texte = texte +"Coord = "+transform.position.ToString()+"  \n";
        //texte=texte+"Rot = \nX="+rtx+"\nY="+rty+"\nZ="+rtz;
        txt.text = texte;
    }

    public void ecrireFichier(string fichier, string data)
    {
        File.AppendAllText(fichier, data + "\r\n");
    }

}
