﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float moveHorizontal = Input.GetAxis ("Horizontal") ;
		float moveVertical = Input.GetAxis ("Vertical") ;
		Vector3 movement = new Vector3 (moveHorizontal / 10.0f, 0.0f, moveVertical / 10.0f) ;
        transform.position = transform.position + movement ;
    }
}
