﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadRotation : MonoBehaviour
{
    public GameObject head;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.localRotation = getHeadRotation(head);
    }

    private Quaternion getHeadRotation(GameObject gameObject)
    {

        return gameObject.transform.rotation;
    }

}
