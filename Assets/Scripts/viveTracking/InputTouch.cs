﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputTouch : MonoBehaviour
{
    public GameObject ecran;

    private Vector3 pos;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        Debug.Log("Nb de doigts HORS DU IF " + Input.touchCount);

        pos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1));

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            
            Debug.Log("Nb de doigts " + Input.touchCount);
            //Debug.Log("Nb de doigts " + Input.touchCount + "// Coordonnées du point touché " + Input.GetTouch(0).position);

            /*Debug.Log("Nb de doigts " + Input.touchCount + "// Coordonnées du point touché " + Input.GetTouch(0).position + 
                     " // Position dans Unity " + (new Vector3(pos.x, pos.y, 0)).ToString());*/
            /*if (touch.phase==TouchPhase.Began) 
            {
                Debug.Log("Began " + touch.position);
            }
            if (touch.phase == TouchPhase.Moved)
            {
                Debug.Log("Moved " + touch.position);
            }
            if (touch.phase == TouchPhase.Ended)
            {
                Debug.Log("Ended " + touch.position);
            }*/

            for (int i = 0; i < Input.touchCount; ++i)
            {
                if (Input.GetTouch(i).phase == TouchPhase.Began)
                {
                    // Construct a ray from the current touch coordinates
                    Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(i).position);

                    // Create a particle if hit
                    if (Physics.Raycast(ray, out hit,3500f))
                    {
                        //Instantiate(particle, transform.position, transform.rotation);
                        //Debug.Log("L'objet " + hit.transform.name + " a été touché");
                        if (hit.collider && ecran != null && hit.transform.name != ecran.transform.name)
                        {
                            Color color = new Color(1f, 1f, 0f, 1f); // jaune
                            hit.collider.GetComponent<Renderer>().material.color = color;

                        }
                    }
                }
            }
        }
        

    }
}

 