﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Distance2 : MonoBehaviour
{
    public GameObject Objet1;
    public GameObject Objet2;
    public float Distance_;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Distance_ = Vector3.Distance(Objet1.transform.position, Objet2.transform.position);
       // Debug.Log("Distace entre CUBE et SPHERE "+ Distance_);
    }
}
