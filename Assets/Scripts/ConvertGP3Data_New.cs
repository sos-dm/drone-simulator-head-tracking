﻿using System;
using UnityEngine;

namespace TobiiGlasses
{
    public class ConvertGP3Data_New
    {


        // Class GazePosition3D
        public class Data { }

        //classe qui fabrique les positions du regard
        public class GazePosition3D : Data
        {
            public string ts { get; set; }
            public string gp3 { get; set; }
            public string s { get; set; }

            public GazePosition3D(JSONObject jo)
            {
                this.gp3 = jo.GetField("gp3").ToString();/* jo.GetField("gp").ToString()*/;
                this.ts = jo.GetField("ts").ToString();
                this.s = jo.GetField("s").ToString();
            }
        }

        //classe qui fabrique l'objet à retourner par CData
        public class DataConverted : Data
        {
            public float[] gp3 { get; set; }
            //public float[] gp3 = new float[3];
            public long ts { get; set; }
            public int s { get; set; }

            public DataConverted(float[] gp3, long ts, int s)
            {
                this.gp3 = gp3;
                this.ts = ts;
                this.s = s;
            }

            
        }

        public static DataConverted CData(string dataReceivedString)
        {
            JSONObject jobject = new JSONObject(dataReceivedString);
            GazePosition3D gazePosition3D = new GazePosition3D(jobject);
            
            
            String coordonnee = gazePosition3D.gp3.Replace("[", "").Replace("]", "").Replace(" ", "");
            string[] stringSeparators = new string[] { "," };
            string[] result = coordonnee.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
            string xString = (string)result.GetValue(0);
            string yString = (string)result.GetValue(1);
            string zString = (string)result.GetValue(2)/*"0"*/;  //ici pour la 2D
            float x = Convert.ToSingle(xString);
            float y = Convert.ToSingle(yString);
            float z = Convert.ToSingle(zString);
            //float x = float.Parse("0,0");
            //Console.WriteLine(x);
            //Console.WriteLine("Detecte Gaze Position 3D " + gp3D + "x"+ xString + "\ny"+ yString + "\nz"+ zString);
            float[] output = new float[3];
            output[0] = x;
            output[1] = y;
            output[2] = z;

            long ts = long.Parse(gazePosition3D.ts);
            int s = int.Parse(gazePosition3D.s);
            DataConverted dataCoverted = new DataConverted(output,ts,s);

            return dataCoverted;
        }
    }
}
