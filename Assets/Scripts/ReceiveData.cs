﻿using System.Net.Sockets;
using System.Text;
using UnityEngine;

namespace TobiiGlasses
{
    public class ReceiveData
    {

        public static string RData(Socket socketData)
        {
            socketData.ReceiveTimeout = 1000;
            byte[] dataReceivedBytes = new byte[1000];
            socketData.Receive(dataReceivedBytes);
            string dataReceivedString = Encoding.ASCII.GetString(dataReceivedBytes);

            Debug.Log("Données recus [1000] : " + dataReceivedString);

            return dataReceivedString;
        }
    }
}
