﻿using System;

namespace TobiiGlasses
{
	public class ConvertPDData
	{

		// Class GazePosition3D
		public class Data { }
		public class PupilDiameter : Data
		{
			public string ts { get; set; }
			public string pd { get; set; }
			public string s { get; set; }
			public string eye{ get; set;}

			public PupilDiameter(JSONObject jo)
			{
				this.pd = jo.GetField("pd").ToString();
				this.ts = jo.GetField("ts").ToString();
				this.s = jo.GetField("s").ToString();
				this.eye = jo.GetField("eye").ToString();
			}
		}
		public static float PDData(string dataReceivedString){
			JSONObject jObject = new JSONObject (dataReceivedString);
			PupilDiameter pDiameter = new PupilDiameter (jObject);
			string diameterString = pDiameter.pd;
			return Convert.ToSingle (diameterString);
		}
	}
}

