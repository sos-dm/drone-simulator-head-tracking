﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: For controlling in-game objects with tracked devices.
//
//=============================================================================

using UnityEngine;
using Valve.VR;

namespace Valve.VR
{
    public class SteamVR_TrackedObject : MonoBehaviour
    {
        public enum EIndex
        {
            None = -1,
            Hmd = (int)OpenVR.k_unTrackedDeviceIndex_Hmd,
            Device1,
            Device2,
            Device3,
            Device4,
            Device5,
            Device6,
            Device7,
            Device8,
            Device9,
            Device10,
            Device11,
            Device12,
            Device13,
            Device14,
            Device15
        }

        public EIndex index;

        [Tooltip("If not set, relative to parent")]
        public Transform origin;

        // moi--------------------
        public bool trackRotation;
        public bool isHead;
        public GameObject Laser;

   

        private Quaternion headRotation;
        public static Vector3 eulerAngle;

        //----------------------------------

        public bool isValid { get; private set; }

        private void OnNewPoses(TrackedDevicePose_t[] poses)
        {
            if (index == EIndex.None)
                return;

            var i = (int)index;

            isValid = false;
            if (poses.Length <= i)
                return;

            if (!poses[i].bDeviceIsConnected)
                return;

            if (!poses[i].bPoseIsValid)
                return;

            isValid = true;

            var pose = new SteamVR_Utils.RigidTransform(poses[i].mDeviceToAbsoluteTracking);

            if (origin != null)
            {
                transform.position = origin.transform.TransformPoint(pose.pos);
                //transform.rotation = origin.rotation * pose.rot; /*version originale*/

                //avec LaserOrigine et LaserEnd---------
                //Laser.transform.rotation = pose.rot;
                
                 //avec le cylindre-------------------------------------
                 Laser.transform.rotation = origin.rotation * pose.rot;
                
                //modification : empécher la rotation de l'écran------------------
                if (trackRotation)
                    transform.rotation = pose.rot;
               else
                    transform.rotation = Quaternion.identity;
            }
            else
            {
                transform.localPosition = pose.pos;
                //transform.localRotation = pose.rot; /*version originale*/

                Laser.transform.rotation = pose.rot;


                //modification : empécher la rotation de l'écran------------------
                if (trackRotation)
                    transform.rotation = pose.rot;
                else
                    transform.rotation = Quaternion.identity;
            }

            ////---------------gérer la rotation de la tete-------------------------------------
            ////--------------------------------------------------------------------------------
            //if (isHead && Laser != null)
            //{
            //    headRotation = /*origin.rotation **/ pose.rot;
            //    eulerAngle = headRotation.eulerAngles;
            //    float angle;
            //    float h = 0;
            //    float x = 0;
            //    float y = 0;
            //    float z = 0;

            //    Vector3 o = GameObject.Find("Origine").transform.position;
            //    Vector3 d = Laser.transform.position;

            //    h = Vector3.Distance(new Vector3(o.x, o.y, 0), new Vector3(d.x, d.y, 0));

            //    angle = Quaternion.Angle(Quaternion.identity, headRotation);

            //    if (headRotation.x > 0)
            //    {
            //        angle = 360- angle;
            //    }

            //    x = h * Mathf.Cos(Mathf.Deg2Rad * angle);
            //    y = h * Mathf.Sin(Mathf.Deg2Rad * angle);
            //    Laser.transform.position = new Vector3(x, y, z);
            //    //Debug.Log(eulerAngle);
            //    Debug.Log("Quaternion " + headRotation + " // Rotation tete " + eulerAngle + " // Angle Degré " + angle.ToString() + eulerAngle + " // Angle Radian " + (angle * Mathf.Deg2Rad).ToString());
            //}
            ////---------------------------------------------------------------------
            ////---------------------------------------------------------------------
        }

        SteamVR_Events.Action newPosesAction;

        SteamVR_TrackedObject()
        {
            newPosesAction = SteamVR_Events.NewPosesAction(OnNewPoses);
        }

        private void Awake()
        {
            OnEnable();
            
        }

        void OnEnable()
        {
            var render = SteamVR_Render.instance;
            if (render == null)
            {
                enabled = false;
                return;
            }

            newPosesAction.enabled = true;
        }

        void OnDisable()
        {
            newPosesAction.enabled = false;
            isValid = false;
        }

        public void SetDeviceIndex(int index)
        {
            if (System.Enum.IsDefined(typeof(EIndex), index))
                this.index = (EIndex)index;
        }
    }
}